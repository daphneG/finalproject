-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Gegenereerd op: 15 jun 2018 om 16:47
-- Serverversie: 10.1.28-MariaDB
-- PHP-versie: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `finalProject`
--

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'ontbijt', '2018-06-09 09:51:14', '2018-06-09 09:51:14'),
(2, 'airfryer', '2018-06-14 15:38:35', '2018-06-14 15:38:35'),
(3, 'groente', '2018-06-14 16:04:05', '2018-06-14 16:04:05'),
(4, 'schijf van vijf', '2018-06-14 16:13:42', '2018-06-14 16:13:42');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `name`, `email`, `comment`, `created_at`, `updated_at`) VALUES
(2, 1, 'Daphne de Groot', 'd.degroot1986@gmail.com', 'test', '2018-06-09 09:55:38', '2018-06-09 09:55:38'),
(3, 2, 'Daphne de Groot', 'd.degroot1986@gmail.com', 'test', '2018-06-11 16:22:44', '2018-06-11 16:22:44');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `contacts`
--

CREATE TABLE `contacts` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastName` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phoneNumber` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `favorites`
--

CREATE TABLE `favorites` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `favorites`
--

INSERT INTO `favorites` (`id`, `user_id`, `recipe_id`, `created_at`, `updated_at`) VALUES
(3, 2, 2, '2018-06-07 17:17:27', '2018-06-07 17:17:27'),
(4, 2, 1, '2018-06-08 10:25:18', '2018-06-08 10:25:18');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ingredients`
--

CREATE TABLE `ingredients` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `quantity` decimal(5,1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ingredients`
--

INSERT INTO `ingredients` (`id`, `recipe_id`, `quantity`, `created_at`, `updated_at`) VALUES
(2, 1, '360.0', '2018-06-11 13:53:18', '2018-06-11 13:53:18'),
(3, 1, '50.0', '2018-06-11 13:53:34', '2018-06-11 13:53:34'),
(4, 1, '3.0', '2018-06-11 13:54:17', '2018-06-11 13:54:17'),
(5, 1, '0.5', '2018-06-11 13:54:41', '2018-06-11 13:54:41'),
(6, 2, '1.0', '2018-06-11 13:58:02', '2018-06-11 13:58:02'),
(7, 2, '200.0', '2018-06-11 13:58:19', '2018-06-11 13:58:19'),
(8, 2, '2.0', '2018-06-11 13:58:34', '2018-06-11 13:58:34'),
(9, 2, '250.0', '2018-06-11 14:00:19', '2018-06-11 14:00:19'),
(10, 2, '2.0', '2018-06-11 14:00:31', '2018-06-11 14:00:31'),
(11, 2, '100.0', '2018-06-11 14:00:46', '2018-06-11 14:00:46'),
(12, 2, '1.0', '2018-06-11 14:01:02', '2018-06-11 14:01:02'),
(13, 2, '0.5', '2018-06-11 14:01:31', '2018-06-11 14:01:31'),
(14, 2, '15.0', '2018-06-11 14:01:44', '2018-06-11 14:01:44'),
(15, 2, '4.0', '2018-06-11 14:02:00', '2018-06-11 14:02:00'),
(16, 3, '50.0', '2018-06-11 14:05:42', '2018-06-11 14:05:42'),
(17, 3, '3.0', '2018-06-11 14:06:03', '2018-06-11 14:06:03'),
(18, 3, '125.0', '2018-06-11 14:06:20', '2018-06-11 14:06:20'),
(19, 3, '2.0', '2018-06-11 14:06:32', '2018-06-11 14:06:32'),
(20, 3, '4.0', '2018-06-11 14:06:51', '2018-06-11 14:06:51'),
(21, 3, '200.0', '2018-06-11 14:07:12', '2018-06-11 14:07:12'),
(22, 3, '100.0', '2018-06-11 14:07:24', '2018-06-11 14:07:24'),
(23, 3, '2.0', '2018-06-11 14:07:37', '2018-06-11 14:07:37'),
(24, 3, '1.0', '2018-06-11 14:07:54', '2018-06-11 14:07:54'),
(25, 4, '150.0', '2018-06-11 14:56:32', '2018-06-11 14:56:32'),
(26, 4, '75.0', '2018-06-11 14:56:46', '2018-06-11 14:56:46'),
(27, 4, '120.0', '2018-06-11 14:57:01', '2018-06-11 14:57:01'),
(28, 4, '120.0', '2018-06-11 14:57:16', '2018-06-11 14:57:16'),
(29, 4, '4.0', '2018-06-11 14:57:34', '2018-06-11 14:57:34'),
(30, 4, '1.0', '2018-06-11 14:57:46', '2018-06-11 14:57:46'),
(31, 4, '5.0', '2018-06-11 14:57:59', '2018-06-11 14:57:59'),
(32, 4, '4.0', '2018-06-11 14:58:17', '2018-06-11 14:58:17'),
(33, 4, '1.0', '2018-06-11 14:58:36', '2018-06-11 14:58:36'),
(34, 5, '1.0', '2018-06-11 15:12:23', '2018-06-11 15:12:23'),
(35, 5, '3.0', '2018-06-11 15:12:43', '2018-06-11 15:12:43'),
(36, 5, '1.0', '2018-06-11 15:12:57', '2018-06-11 15:12:57'),
(37, 5, '500.0', '2018-06-11 15:47:25', '2018-06-11 15:47:25'),
(38, 5, '1.0', '2018-06-11 15:47:41', '2018-06-11 15:47:41'),
(39, 5, '1.0', '2018-06-11 15:48:04', '2018-06-11 15:48:04'),
(40, 5, '400.0', '2018-06-11 15:48:22', '2018-06-11 15:48:22'),
(41, 5, '200.0', '2018-06-11 15:48:44', '2018-06-11 15:48:44'),
(42, 5, '1.0', '2018-06-11 15:48:57', '2018-06-11 15:48:57'),
(43, 5, '1.0', '2018-06-11 15:49:14', '2018-06-11 15:49:14'),
(44, 5, '1.0', '2018-06-11 15:49:28', '2018-06-11 15:49:28'),
(45, 5, '1.0', '2018-06-11 15:49:45', '2018-06-11 15:49:45'),
(46, 5, '1.0', '2018-06-11 15:50:01', '2018-06-11 15:50:01'),
(47, 5, '1.0', '2018-06-11 15:50:22', '2018-06-11 15:50:22'),
(48, 5, '1.0', '2018-06-11 15:50:46', '2018-06-11 15:50:46'),
(49, 5, '1.0', '2018-06-11 15:51:02', '2018-06-11 15:51:02'),
(50, 6, '300.0', '2018-06-14 16:42:00', '2018-06-14 16:42:00'),
(51, 6, '80.0', '2018-06-14 16:42:19', '2018-06-14 16:42:19'),
(52, 6, '250.0', '2018-06-14 16:42:33', '2018-06-14 16:42:33'),
(53, 6, '3.0', '2018-06-14 16:42:44', '2018-06-14 16:42:44'),
(54, 6, '200.0', '2018-06-14 16:43:27', '2018-06-14 16:43:27'),
(55, 6, '1.0', '2018-06-14 16:43:41', '2018-06-14 16:43:41'),
(56, 7, '300.0', '2018-06-14 16:48:38', '2018-06-14 16:48:38'),
(57, 7, '200.0', '2018-06-14 16:48:56', '2018-06-14 16:48:56'),
(58, 7, '1.0', '2018-06-14 16:49:12', '2018-06-14 16:49:12'),
(59, 7, '2.0', '2018-06-14 16:49:27', '2018-06-14 16:49:27'),
(60, 7, '1.0', '2018-06-14 16:49:42', '2018-06-14 16:49:42'),
(61, 7, '1.0', '2018-06-14 16:50:00', '2018-06-14 16:50:00'),
(62, 7, '2.0', '2018-06-14 16:50:28', '2018-06-14 16:50:28'),
(63, 7, '1.0', '2018-06-14 16:50:46', '2018-06-14 16:50:46'),
(64, 7, '1.0', '2018-06-14 16:50:59', '2018-06-14 16:50:59'),
(65, 7, '1.0', '2018-06-14 16:51:19', '2018-06-14 16:51:19'),
(66, 7, '1.0', '2018-06-14 16:51:35', '2018-06-14 16:51:35'),
(67, 8, '150.0', '2018-06-14 16:55:11', '2018-06-14 16:55:11'),
(68, 8, '1.0', '2018-06-14 16:55:27', '2018-06-14 16:55:27'),
(69, 8, '3.0', '2018-06-14 16:55:40', '2018-06-14 16:55:40'),
(70, 8, '1.0', '2018-06-14 16:55:53', '2018-06-14 16:55:53'),
(71, 8, '1.0', '2018-06-14 16:56:08', '2018-06-14 16:56:08'),
(72, 8, '0.5', '2018-06-14 16:56:42', '2018-06-14 16:56:42'),
(73, 8, '1.0', '2018-06-14 16:57:03', '2018-06-14 16:57:03'),
(74, 9, '1.0', '2018-06-14 17:00:15', '2018-06-14 17:00:15'),
(75, 9, '50.0', '2018-06-14 17:00:32', '2018-06-14 17:00:32'),
(76, 9, '2.0', '2018-06-14 17:00:58', '2018-06-14 17:00:58'),
(77, 9, '75.0', '2018-06-14 17:01:11', '2018-06-14 17:01:11'),
(78, 9, '1.0', '2018-06-14 17:01:56', '2018-06-14 17:01:56'),
(79, 9, '1.0', '2018-06-14 17:02:11', '2018-06-14 17:02:11'),
(80, 9, '1.0', '2018-06-14 17:02:21', '2018-06-14 17:02:21'),
(81, 10, '4.0', '2018-06-14 17:04:54', '2018-06-14 17:04:54'),
(82, 10, '0.5', '2018-06-14 17:05:12', '2018-06-14 17:05:12'),
(83, 10, '2.0', '2018-06-14 17:05:25', '2018-06-14 17:05:25'),
(84, 10, '1.0', '2018-06-14 17:05:41', '2018-06-14 17:05:41'),
(85, 10, '2.0', '2018-06-14 17:06:18', '2018-06-14 17:06:18'),
(86, 10, '1.0', '2018-06-14 17:06:31', '2018-06-14 17:06:31'),
(87, 10, '1.0', '2018-06-14 17:06:45', '2018-06-14 17:06:45'),
(88, 10, '1.0', '2018-06-14 17:06:55', '2018-06-14 17:06:55'),
(89, 11, '2.0', '2018-06-14 17:10:46', '2018-06-14 17:10:46'),
(90, 11, '1.0', '2018-06-14 17:11:08', '2018-06-14 17:11:08'),
(91, 11, '1.0', '2018-06-14 17:11:22', '2018-06-14 17:11:22'),
(92, 11, '50.0', '2018-06-14 17:11:39', '2018-06-14 17:11:39'),
(93, 11, '2.0', '2018-06-14 17:11:56', '2018-06-14 17:11:56'),
(94, 11, '1.0', '2018-06-14 17:12:27', '2018-06-14 17:12:27'),
(95, 11, '1.0', '2018-06-14 17:12:41', '2018-06-14 17:12:41'),
(96, 11, '1.0', '2018-06-14 17:12:53', '2018-06-14 17:12:53'),
(97, 12, '200.0', '2018-06-14 17:17:52', '2018-06-14 17:17:52'),
(98, 12, '1.0', '2018-06-14 17:18:08', '2018-06-14 17:18:08'),
(99, 12, '200.0', '2018-06-14 17:18:21', '2018-06-14 17:18:21'),
(100, 12, '150.0', '2018-06-14 17:18:36', '2018-06-14 17:18:36'),
(101, 12, '1.0', '2018-06-14 17:18:50', '2018-06-14 17:18:50'),
(102, 12, '2.0', '2018-06-14 17:19:05', '2018-06-14 17:19:05'),
(103, 12, '185.0', '2018-06-14 17:19:21', '2018-06-14 17:19:21'),
(104, 12, '150.0', '2018-06-14 17:19:41', '2018-06-14 17:19:41'),
(105, 12, '100.0', '2018-06-14 17:19:54', '2018-06-14 17:19:54'),
(106, 13, '250.0', '2018-06-14 17:22:30', '2018-06-14 17:22:30'),
(107, 13, '250.0', '2018-06-14 17:22:43', '2018-06-14 17:22:43'),
(108, 13, '125.0', '2018-06-14 17:22:56', '2018-06-14 17:22:56'),
(109, 13, '125.0', '2018-06-14 17:23:09', '2018-06-14 17:23:09'),
(110, 13, '3.0', '2018-06-14 17:23:27', '2018-06-14 17:23:27'),
(111, 13, '1.0', '2018-06-14 17:24:13', '2018-06-14 17:24:13'),
(112, 13, '1.0', '2018-06-14 17:24:25', '2018-06-14 17:24:25'),
(113, 13, '1.0', '2018-06-14 17:25:26', '2018-06-14 17:25:26'),
(114, 14, '300.0', '2018-06-14 17:28:25', '2018-06-14 17:28:25'),
(115, 14, '120.0', '2018-06-14 17:28:37', '2018-06-14 17:28:37'),
(116, 14, '1.0', '2018-06-14 17:28:51', '2018-06-14 17:28:51'),
(117, 14, '125.0', '2018-06-14 17:29:05', '2018-06-14 17:29:05'),
(118, 14, '2.0', '2018-06-14 17:29:29', '2018-06-14 17:29:29'),
(119, 14, '3.0', '2018-06-14 17:29:41', '2018-06-14 17:29:41'),
(120, 14, '7.0', '2018-06-14 17:29:56', '2018-06-14 17:29:56'),
(121, 14, '5.0', '2018-06-14 17:30:08', '2018-06-14 17:30:08'),
(122, 15, '1.0', '2018-06-14 17:39:10', '2018-06-14 17:39:10'),
(123, 15, '100.0', '2018-06-14 17:39:28', '2018-06-14 17:39:28'),
(124, 15, '100.0', '2018-06-14 17:39:54', '2018-06-14 17:39:54'),
(125, 16, '3.0', '2018-06-14 17:45:44', '2018-06-14 17:45:44'),
(126, 16, '3.0', '2018-06-14 17:46:01', '2018-06-14 17:46:01'),
(127, 16, '100.0', '2018-06-14 17:46:13', '2018-06-14 17:46:13'),
(128, 16, '75.0', '2018-06-14 17:46:35', '2018-06-14 17:46:35'),
(129, 16, '100.0', '2018-06-14 17:46:46', '2018-06-14 17:46:46'),
(130, 16, '2.0', '2018-06-14 17:47:03', '2018-06-14 17:47:03'),
(131, 16, '1.0', '2018-06-14 17:47:16', '2018-06-14 17:47:16'),
(132, 16, '100.0', '2018-06-14 17:47:50', '2018-06-14 17:47:50'),
(133, 16, '1.0', '2018-06-14 17:48:07', '2018-06-14 17:48:07'),
(134, 16, '75.0', '2018-06-14 17:48:22', '2018-06-14 17:48:22'),
(135, 16, '5.0', '2018-06-14 17:48:34', '2018-06-14 17:48:34');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ingredient_product`
--

CREATE TABLE `ingredient_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `ingredient_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ingredient_product`
--

INSERT INTO `ingredient_product` (`id`, `product_id`, `ingredient_id`) VALUES
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 5),
(6, 6, 6),
(7, 7, 7),
(8, 8, 8),
(9, 9, 9),
(10, 10, 10),
(11, 11, 11),
(12, 12, 12),
(13, 13, 13),
(14, 14, 14),
(15, 15, 15),
(16, 16, 16),
(17, 17, 17),
(18, 18, 18),
(19, 19, 19),
(20, 20, 20),
(21, 21, 21),
(22, 22, 22),
(23, 23, 23),
(24, 24, 24),
(25, 25, 25),
(26, 26, 26),
(27, 27, 27),
(28, 28, 28),
(29, 29, 29),
(30, 30, 30),
(31, 31, 31),
(32, 9, 32),
(33, 1, 33),
(34, 34, 34),
(35, 35, 35),
(36, 36, 36),
(37, 37, 37),
(38, 38, 38),
(39, 39, 39),
(40, 40, 40),
(41, 41, 41),
(42, 42, 42),
(43, 43, 43),
(44, 44, 44),
(45, 13, 45),
(46, 45, 46),
(47, 46, 47),
(48, 47, 48),
(49, 48, 49),
(50, 58, 50),
(51, 26, 51),
(52, 31, 52),
(53, 59, 53),
(54, 60, 54),
(55, 61, 55),
(56, 62, 56),
(57, 63, 57),
(58, 64, 58),
(59, 35, 59),
(60, 46, 60),
(61, 65, 61),
(62, 20, 62),
(63, 48, 63),
(64, 47, 64),
(65, 8, 65),
(66, 66, 66),
(67, 67, 67),
(68, 41, 68),
(69, 35, 69),
(70, 10, 70),
(71, 68, 71),
(72, 69, 72),
(73, 70, 73),
(74, 64, 74),
(75, 71, 75),
(76, 72, 76),
(77, 73, 77),
(78, 74, 78),
(79, 48, 79),
(80, 47, 80),
(81, 75, 81),
(82, 76, 82),
(83, 35, 83),
(84, 69, 84),
(85, 77, 85),
(86, 65, 86),
(87, 48, 87),
(88, 47, 88),
(89, 78, 89),
(90, 79, 90),
(91, 12, 91),
(92, 71, 92),
(93, 80, 93),
(94, 8, 94),
(95, 48, 95),
(96, 47, 96),
(97, 18, 97),
(98, 81, 98),
(99, 26, 99),
(100, 50, 100),
(101, 59, 101),
(102, 20, 102),
(103, 82, 103),
(104, 83, 104),
(105, 31, 105),
(106, 84, 106),
(107, 31, 107),
(108, 85, 108),
(109, 86, 109),
(110, 50, 110),
(111, 87, 111),
(112, 88, 112),
(113, 89, 113),
(114, 49, 114),
(115, 30, 115),
(116, 20, 116),
(117, 26, 117),
(118, 50, 118),
(119, 50, 119),
(120, 90, 120),
(121, 48, 121),
(122, 91, 122),
(123, 92, 123),
(124, 93, 124),
(125, 94, 125),
(126, 20, 126),
(127, 82, 127),
(128, 95, 128),
(129, 30, 129),
(130, 96, 130),
(131, 88, 131),
(132, 97, 132),
(133, 98, 133),
(134, 60, 134),
(135, 1, 135);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `ingredient_unit`
--

CREATE TABLE `ingredient_unit` (
  `id` int(10) UNSIGNED NOT NULL,
  `ingredient_id` int(11) NOT NULL,
  `unit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `ingredient_unit`
--

INSERT INTO `ingredient_unit` (`id`, `ingredient_id`, `unit_id`) VALUES
(1, 2, 5),
(2, 3, 5),
(3, 4, 10),
(4, 5, 11),
(5, 6, 7),
(6, 7, 5),
(7, 8, 10),
(8, 9, 5),
(9, 10, 6),
(10, 11, 5),
(11, 12, 7),
(12, 13, 7),
(13, 14, 5),
(14, 15, 6),
(16, 16, 5),
(17, 17, 6),
(18, 18, 5),
(19, 19, 6),
(20, 20, 6),
(21, 21, 2),
(22, 22, 5),
(23, 23, 12),
(24, 24, 9),
(25, 25, 5),
(26, 26, 5),
(27, 27, 5),
(28, 28, 5),
(29, 29, 13),
(30, 30, 14),
(31, 31, 14),
(32, 32, 6),
(33, 33, 8),
(34, 34, 19),
(35, 35, 6),
(36, 36, 7),
(37, 37, 5),
(38, 38, 7),
(39, 39, 18),
(40, 40, 5),
(41, 41, 5),
(42, 42, 17),
(43, 43, 16),
(44, 44, 11),
(45, 45, 7),
(46, 46, 15),
(47, 47, 10),
(48, 48, 11),
(49, 49, 11),
(50, 50, 5),
(51, 51, 5),
(52, 52, 2),
(54, 53, 20),
(55, 54, 5),
(56, 55, 16),
(57, 56, 5),
(58, 57, 5),
(59, 58, 7),
(60, 59, 6),
(61, 60, 10),
(62, 61, 10),
(63, 62, 6),
(64, 63, 18),
(65, 64, 18),
(66, 65, 11),
(67, 66, 11),
(68, 67, 5),
(69, 68, 16),
(70, 69, 6),
(71, 70, 7),
(72, 71, 16),
(73, 72, 7),
(74, 73, 7),
(75, 74, 7),
(76, 75, 5),
(77, 76, 6),
(78, 77, 5),
(79, 78, 21),
(80, 79, 18),
(81, 80, 18),
(82, 81, 6),
(83, 82, 7),
(84, 83, 6),
(85, 84, 7),
(86, 85, 10),
(87, 86, 10),
(88, 87, 18),
(89, 88, 18),
(90, 89, 6),
(91, 90, 7),
(92, 91, 7),
(93, 92, 5),
(94, 93, 10),
(95, 94, 10),
(96, 95, 18),
(97, 96, 18),
(98, 97, 5),
(99, 98, 11),
(100, 99, 5),
(101, 100, 5),
(102, 101, 11),
(103, 102, 6),
(104, 103, 5),
(105, 104, 5),
(106, 105, 2),
(107, 106, 5),
(108, 107, 2),
(109, 108, 5),
(110, 109, 5),
(111, 110, 10),
(112, 111, 22),
(114, 112, 11),
(115, 113, 23),
(116, 114, 5),
(117, 115, 2),
(118, 116, 7),
(119, 117, 5),
(120, 118, 10),
(121, 119, 11),
(122, 120, 5),
(123, 121, 5),
(124, 122, 7),
(125, 123, 2),
(126, 124, 5),
(127, 125, 6),
(128, 126, 6),
(129, 127, 5),
(130, 128, 5),
(131, 129, 2),
(132, 130, 11),
(133, 131, 11),
(134, 132, 5),
(135, 133, 21),
(136, 134, 5),
(137, 135, 10);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_05_23_132255_create_roles_table', 1),
(4, '2018_05_23_140054_create_role_user_table', 1),
(5, '2018_05_24_174958_create_photos_table', 1),
(6, '2018_05_24_175129_create_recipes_table', 1),
(7, '2018_05_24_175407_create_ingredients_table', 1),
(8, '2018_05_24_175906_create_products_table', 1),
(9, '2018_05_24_180215_create_units_table', 1),
(10, '2018_05_24_182718_create_ingredient_product_table', 1),
(11, '2018_05_24_182904_create_ingredient_unit_table', 1),
(12, '2018_05_29_181231_create_steps_table', 1),
(13, '2018_06_03_130527_create_posts_table', 1),
(14, '2018_06_03_130539_create_categories_table', 1),
(15, '2018_06_03_130552_create_comments_table', 1),
(16, '2018_06_07_143502_create_favorites_table', 1),
(17, '2018_06_10_131859_create_contacts_table', 2);

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(11) DEFAULT NULL,
  `post_id` int(11) DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `photos`
--

INSERT INTO `photos` (`id`, `recipe_id`, `post_id`, `path`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 'ontbijtCrumble.jpg', '2018-06-07 14:36:25', '2018-06-07 14:36:25'),
(2, 2, NULL, 'lunchwrap.jpg', '2018-06-07 17:14:41', '2018-06-07 17:14:41'),
(3, 3, NULL, 'quicheGeitenkaas.jpg', '2018-06-08 11:46:09', '2018-06-08 11:46:09'),
(6, 4, NULL, 'chocoladetaart5.jpg', '2018-06-11 14:56:07', '2018-06-11 14:56:07'),
(7, 5, NULL, 'ChiliConCarne.jpg', '2018-06-11 15:08:33', '2018-06-11 15:08:33'),
(8, NULL, 3, 'airfryer.jpg', '2018-06-14 15:44:43', '2018-06-14 15:44:43'),
(9, NULL, 4, 'groente.png', '2018-06-14 16:09:45', '2018-06-14 16:09:45'),
(10, NULL, 5, 'Schijf-van-Vijf-klein.jpg', '2018-06-14 16:18:58', '2018-06-14 16:18:58'),
(11, 6, NULL, 'monchou taart.jpg', '2018-06-14 16:41:37', '2018-06-14 16:41:37'),
(12, 7, NULL, 'salade met zalm.jpg', '2018-06-14 16:48:18', '2018-06-14 16:48:18'),
(13, 8, NULL, 'mexicaanse macaroni-salade.jpg', '2018-06-14 16:54:54', '2018-06-14 16:54:54'),
(14, 9, NULL, 'courgette carpaccio.jpg', '2018-06-14 16:59:55', '2018-06-14 16:59:55'),
(15, 10, NULL, 'groente tosti.jpg', '2018-06-14 17:04:38', '2018-06-14 17:04:38'),
(16, 11, NULL, 'sandwich kip.jpg', '2018-06-14 17:10:19', '2018-06-14 17:10:19'),
(17, 12, NULL, 'toffee cake.jpg', '2018-06-14 17:17:35', '2018-06-14 17:17:35'),
(18, 13, NULL, 'tiramisu met roodfruit.jpg', '2018-06-14 17:22:17', '2018-06-14 17:22:17'),
(19, 14, NULL, 'brioche.jpg', '2018-06-14 17:28:13', '2018-06-14 17:28:13'),
(20, NULL, 6, 'groene smootie.jpg', '2018-06-14 17:35:56', '2018-06-14 17:35:56'),
(21, 15, NULL, 'smoothie bowl.jpg', '2018-06-14 17:38:58', '2018-06-14 17:38:58'),
(22, 16, NULL, 'carrotcake .jpg', '2018-06-14 17:45:22', '2018-06-14 17:45:22');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `posts`
--

INSERT INTO `posts` (`id`, `category_id`, `title`, `body`, `created_at`, `updated_at`) VALUES
(3, 2, 'Kunnen wij ons nog een leven voorstellen zonder airfryer?', '<h3>Wat is een Airfryer</h3>\r\n<p>We kennen allemaal wel de ouderwetse frituurpan. Niets lekkerder dan een heerlijke snack uit schoon frituurvet. Het nadeel is natuurlijk dat het verre van gezond is. We moeten voorop stellen: snacken is eigenlijk nooit gezond, maar als je dan een keer wil snacken, dan kan dat redelijk verantwoord met de Airfryer.</p>\r\n<p>De Airfryer gaart je voedsel met behulp van hete lucht. Dat had je waarschijnlijk zelf al geraden, afgaand op de naam van het apparaat. Als je helemaal wilt weten hoe de Airfryer precies werkt, dan is dit een heel interessant artikel.</p>\r\n\r\n<h3>De voordelen van een Airfryer</h3>\r\n<p>Waar je in een gewone frituurpan alleen maar friet, snacks en oliebollen kunt bakken, kun je met een Airfryer helemaal los gaan! Je kan er vlees, groenten, frietjes en zelfs brownies in bereiden. Maar er zijn nog veel meer voordelen:</p>\r\n\r\n<ul>\r\n<li>Vind je frituren veel werk, dan zul je erg blij zijn met een Airfryer: geen gedoe met het verschonen van frituurvet!\r\n</li>\r\n<li>Sommige mensen vinden koken met een frituurpan maar spannend: die kokend hete olie blijft natuurlijk een risico. Met de Airfryer hoef je je daar geen zorgen over te maken. Het is een zeer veilige manier van koken.\r\n</li>\r\n<li>Nooit meer blauwbekken in het schuurtje in de tuin of op het balkon: met de Airfryer heb je geen vieze frituurluchtjes in je huis.\r\n</li>\r\n<li>Geen frituurvet meer kopen: dat scheelt een hoop geld!\r\n</li>\r\n<li>Er zijn enorm veel recepten te vinden voor de Airfryer, onder andere op Pinterest\r\n</li>\r\n<li>Veel gezonder dan een gewone frituurpan doordat je geen olie gebruikt\r\n</li>\r\n<li>Snacks en frietjes zijn minder vet dan uit de frituurpan\r\n</li>\r\n<li>Snacks en frietjes zijn minder vet dan uit de frituurpan\r\n</li>\r\n<li>Je kan ook makkelijk broodjes warm maken in de Airfryer. Sommige mensen verkiezen de Airfryer hiervoor zelfs boven hun oven!\r\n</li>\r\n<li>Handig te gebruiken als apart apparaat voor mensen die glutenvrij eten en geen kans op kruisbesmetting willen lopen\r\n</li>\r\n</ul>', '2018-06-14 15:44:43', '2018-06-14 16:02:11'),
(4, 3, 'Recepten met groenten in de hoofdrol', '<strong>Groenten zijn niet alleen gezond, maar ook beter voor het klimaat dan vlees. Maar hoe maak je ze klaar? Met deze recepten draai jij je hand niet meer om voor spinazie, bloemkool, koolrabi en bonen.</strong>\r\n\r\n<p>Groente belast het milieu minder dan vlees. Dus als je meer groente eet en minder vlees, levert dat klimaatwinst op. Om deze boodschap smaak te geven, heeft Milieu Centraal koks en foodies zoals Loethe Olthuis (de Volkskrant), Bas Cloo (Dutch Cuisine) en Linda Peek (lekkeretenmetlinda.nl) gevraagd om recepten te maken met groenten in de hoofdrol. De medewerkers van Milieu Centraal gingen je alvast voor: veel recepten zijn door hen getest, goedgekeurd en op de foto gezet!</p>\r\n\r\n<h3>Probeer de recepten meteen!</h3>\r\n\r\n<p>Van zomerse salade tot winterse stoofpot: laat je inspireren door de recepten met groente in de basis. Let wel op dat de recepten kiest die passen bij het seizoen.  Bijvoorbeeld:</p>\r\n\r\n<ul>\r\n<li>Koolrabi recepten</li>\r\n<li>Spinazie recepten</li>\r\n<li>Bloemkool recepten</li>\r\n<li>Peulvruchten recepten</li>\r\n</ul>\r\n\r\n<h3>Winst voor het klimaat</h3>\r\n\r\n<p>Meer groente eten en minder vlees is goed voor het klimaat. Een gemiddelde maaltijd van Nederlanders bestaat voor 70 procent uit groenten en aardappel, pasta of rijst en 30 procent vlees. Een kleine verschuiving naar meer groenten en minder vlees geeft al veel winst voor het klimaat.</p>', '2018-06-14 16:09:45', '2018-06-14 16:13:09'),
(5, 4, '5 tips om gezonder te eten (waar we ons massaal niet aan houden)', '<strong>Onlangs publiceerde het RIVM onderzoek naar voedingsgewoonten van volwassenen in Nederland en vergeleek deze cijfers met de aanbevelingen in de Richtlijnen Goede Voeding van de Gezondheidsraad. Daar bleek nogal een verschil tussen wat we zouden moeten eten en wat we in het echt eten.</strong>\r\n\r\n<h3>De meest opmerkelijke verschillen op een rij in 5 praktische tips:</h3>\r\n\r\n<ol>\r\n<li><strong>Eet meer groente en fruit: 200-250 gram per dag ieder </strong><p>Slechts 1 op de 7 volwassenen (15%) eet voldoende groente en fruit (200 gram/dag); we eten slechts 139 gram groente en zelfs maar 113 gram fruit. Bij kinderen is het vaak nog erger gesteld.</p></li>\r\n\r\n<li><strong>Eet meer (ongezouten) noten </strong><p>Het advies is om een handje noten (15 gram) per dag te eten. We eten slechts ongeveer een kwart hiervan: 4 gram. Slechts 7% van de volwassenen eet het aanbevolen handje noten per dag – dat terwijl pindakaas (gemaakt van 100% pinda’s) ook gewoon meetelt.</p></li>\r\n<li><strong>Eet wekelijks peulvruchten </strong><p>Peulvruchten blijken we gemiddeld slechts eens in de drie weken te eten.</p></li>\r\n<li><strong>Drink zo min mogelijk suikerhoudende dranken</strong><p>We drinken er veel te veel van, gemiddeld 337 gram (pakweg anderhalf glas) per dag. Mannen tussen 19 en 51 jaar zelfs bijna een halve liter per dag.</p></li>\r\n<li><strong>Eet minder zout </strong><p>Terwijl de aanbevolen maximum hoeveelheid zout 6 gram per dag is, eten we er gemiddeld meer dan 8. De helft van de mannen zelfs meer dan 9,7 gram per dag. Slechts 1 op de 33 mannen (3%) en 1 op de 5 vrouwen (19%), eet niet te veel zout. Het overgrote deel (80%) van het zout dat wordt geconsumeerd is toegevoegd aan producten in de supermarkt. Let dus op bij wat je koopt.</p></li>\r\n</ol>\r\n\r\n<p>Over het algemeen eet je gezond als je veel vers, plantaardig en onbewerkt eet, met variatie.</p>', '2018-06-14 16:18:58', '2018-06-14 16:21:05'),
(6, 1, 'Variëren met groene smoothies', '<p>Wij vinden het wel eens lastig om genoeg groenten en fruit te eten. Met behulp van een groene smoothie is dat helemaal niet zo moeilijk. Het kleurtje is misschien even wennen maar je proeft de groenten helemaal niet terug in deze lekkere smoothie. Daarnaast heb je een smoothie in 10 minuten gemaakt en is het meteen een goed begin van de dag. Wil jij nou ook een groene smoothie maken? Wij laten je zien hoe je in 4 simpele stappen een lekkere groen(t)e smoothie op tafel zet.</p>\r\n\r\n<p><strong>Stap 1: fruit\r\n</strong>Kies als eerste uit welk fruit je wilt gebruiken. Wat dacht je van banaan, frambozen, aardbeien, meloen of kiwi? Uiteraard kun je ook meerdere fruitsoorten kiezen. Zelf vind ik een combinatie van banaan en aardbeien erg lekker. Je kunt ook kiezen voor voorgesneden diepvriesfruit om het jezelf helemaal gemakkelijk te maken.</p>\r\n\r\n<p><strong>Stap 2: groenten\r\n</strong>Vervolgens kies je welke groenten je wilt gebruiken. Zelf vindt ik spinazie, boerenkool, avocado, bleekselderij en komkommer aanraders. Het klinkt misschien niet zo lekker om groenten toe te voegen aan je smoothie maar rauwe groenten zoals spinazie en boerenkool proef je eigenlijk helemaal niet terug. Begin bijvoorbeeld met een handje spinazie en bouw het langzaam op. Uiteindelijk kun je gaan voor bijvoorbeeld de helft fruit en de helft groenten.</p>\r\n\r\n<p><strong>Stap 3: voeg iets vloeibaars toe\r\n</strong>Aan je smoothie moet je ook nog iets vloeibaars toevoegen om gemakkelijk te kunnen opdrinken. Je kunt bijvoorbeeld kiezen voor (kokos)water, (amandel)melk, yoghurt of koude thee. Voeg een flinke scheut toe aan de smoothie en mix alles goed door elkaar. Als de smoothie nog te dik is, kun je wat extra toevoegen.</p>\r\n\r\n<p><strong>Stap 4: extra smaak door kruiden en/of specerijen\r\n</strong>Door simpelweg kruiden en/of specerijen aan je smoothie toe te voegen, kun je je smoothie net een andere smaak of een beetje extra smaak geven. Wat dacht je bijvoorbeeld van peterselie, koriander of basilicum. Kaneel en vanille geven een meer zoetere smaak aan je smoothie. Ook een snufje cayennepeper is een lekkere toevoeging. Wij gebruiken zelf de Hoogwaardige Kruiden van Euroma. Deze kruiden zijn lekker van smaak en bevatten verder geen toevoegingen. Het zijn pure kruiden die een bijzonder proces ondergaan. De kruiden worden diepgevroren en onder druk gebracht. Hierdoor blijven de smaak, geur en kleur behouden en kun je ze lang bewaren. Altijd fijn om een hoop kruiden op voorraad te hebben om je smoothies maar ook gerechten lekker op smaak te brengen.</p>', '2018-06-14 17:35:56', '2018-06-14 17:35:56');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `products`
--

INSERT INTO `products` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'poedersuiker', '2018-06-07 17:14:52', '2018-06-07 17:14:52'),
(2, 'appelcompote met stukjes appel', '2018-06-11 13:50:18', '2018-06-11 13:50:18'),
(3, 'ongezouten pecannoten', '2018-06-11 13:50:31', '2018-06-11 13:50:31'),
(4, 'vezelrijke spelmout havervlokken', '2018-06-11 13:50:41', '2018-06-11 13:50:41'),
(5, 'kardemom (gemalen)', '2018-06-11 13:50:49', '2018-06-11 13:50:49'),
(6, 'limoen', '2018-06-11 13:55:17', '2018-06-11 13:55:17'),
(7, 'zuivelspread', '2018-06-11 13:55:28', '2018-06-11 13:55:28'),
(8, 'vloeibare honing', '2018-06-11 13:55:35', '2018-06-11 13:55:35'),
(9, 'aardbeien (verse)', '2018-06-11 13:55:47', '2018-06-11 13:55:47'),
(10, 'avocado\'s (eetrijp)', '2018-06-11 13:56:03', '2018-06-11 13:56:03'),
(11, 'ongezouten cashewnoten', '2018-06-11 13:56:11', '2018-06-11 13:56:11'),
(12, 'mango (eetrijp)', '2018-06-11 13:56:24', '2018-06-11 13:56:24'),
(13, 'rode peper', '2018-06-11 13:56:33', '2018-06-11 13:56:33'),
(14, 'koriander (verse)', '2018-06-11 13:56:50', '2018-06-11 13:56:50'),
(15, 'tortillawraps', '2018-06-11 13:57:11', '2018-06-11 13:57:11'),
(16, 'ongebrande ongezouten notenmix', '2018-06-11 14:03:55', '2018-06-11 14:03:55'),
(17, 'sjalotten', '2018-06-11 14:04:05', '2018-06-11 14:04:05'),
(18, 'verse dadels', '2018-06-11 14:04:17', '2018-06-11 14:04:17'),
(19, 'conference peren', '2018-06-11 14:04:23', '2018-06-11 14:04:23'),
(20, 'eieren (middelgrote)', '2018-06-11 14:04:32', '2018-06-11 14:04:32'),
(21, 'slagroom (verse)', '2018-06-11 14:04:42', '2018-06-11 14:04:42'),
(22, 'geitenkaas jong belegen 50+', '2018-06-11 14:04:49', '2018-06-11 14:04:49'),
(23, 'rozemarijn', '2018-06-11 14:05:01', '2018-06-11 14:05:01'),
(24, 'bladerdeeg (vers)', '2018-06-11 14:05:18', '2018-06-11 14:05:18'),
(25, 'volkorenbiscuit', '2018-06-11 14:16:08', '2018-06-11 14:16:08'),
(26, 'boter', '2018-06-11 14:16:14', '2018-06-11 14:16:14'),
(27, 'witte chocolade', '2018-06-11 14:16:21', '2018-06-11 14:16:21'),
(28, 'extra bitter chocolade', '2018-06-11 14:16:27', '2018-06-11 14:16:27'),
(29, 'witte gelatine', '2018-06-11 14:16:41', '2018-06-11 14:16:41'),
(30, 'melk', '2018-06-11 14:53:59', '2018-06-11 14:53:59'),
(31, 'slagroom', '2018-06-11 14:54:20', '2018-06-11 14:54:20'),
(32, 'aardbeien (verse)', '2018-06-11 14:54:28', '2018-06-11 14:54:28'),
(33, 'poedersuiker', '2018-06-11 14:54:34', '2018-06-11 14:54:34'),
(34, 'knoflook', '2018-06-11 15:09:59', '2018-06-11 15:09:59'),
(35, 'tomaten', '2018-06-11 15:10:11', '2018-06-11 15:10:11'),
(36, 'ui', '2018-06-11 15:10:15', '2018-06-11 15:10:15'),
(37, 'rundergehakt', '2018-06-11 15:10:21', '2018-06-11 15:10:21'),
(38, 'paprika', '2018-06-11 15:10:28', '2018-06-11 15:10:28'),
(39, 'cayennepeper', '2018-06-11 15:10:41', '2018-06-11 15:10:41'),
(40, 'kidneybonen', '2018-06-11 15:10:48', '2018-06-11 15:10:48'),
(41, 'maiskorrels', '2018-06-11 15:10:58', '2018-06-11 15:10:58'),
(42, 'rundvleesbouillon', '2018-06-11 15:11:09', '2018-06-11 15:11:09'),
(43, 'tomatenpuree', '2018-06-11 15:11:18', '2018-06-11 15:11:18'),
(44, 'komijn', '2018-06-11 15:11:24', '2018-06-11 15:11:24'),
(45, 'tortilla chips', '2018-06-11 15:11:37', '2018-06-11 15:11:37'),
(46, 'olijfolie', '2018-06-11 15:11:48', '2018-06-11 15:11:48'),
(47, 'peper', '2018-06-11 15:11:55', '2018-06-11 15:11:55'),
(48, 'zout', '2018-06-11 15:11:58', '2018-06-11 15:11:58'),
(49, 'bloem', '2018-06-14 16:34:39', '2018-06-14 16:34:39'),
(50, 'suiker', '2018-06-14 16:34:47', '2018-06-14 16:34:47'),
(51, 'zout', '2018-06-14 16:34:54', '2018-06-14 16:34:54'),
(52, 'ongezouten boter', '2018-06-14 16:35:17', '2018-06-14 16:35:17'),
(58, 'bastogne koeken', '2018-06-14 16:38:00', '2018-06-14 16:38:00'),
(59, 'vanille suiker', '2018-06-14 16:38:12', '2018-06-14 16:38:12'),
(60, 'monchou', '2018-06-14 16:38:19', '2018-06-14 16:38:19'),
(61, 'vlaaifruit kersen', '2018-06-14 16:38:40', '2018-06-14 16:38:40'),
(62, 'krieltjes', '2018-06-14 16:46:35', '2018-06-14 16:46:35'),
(63, '(warm)gerookte zalm', '2018-06-14 16:46:48', '2018-06-14 16:46:48'),
(64, 'courgette', '2018-06-14 16:46:56', '2018-06-14 16:46:56'),
(65, 'italiaanse kruiden', '2018-06-14 16:50:00', '2018-06-14 16:50:00'),
(66, 'mosterd', '2018-06-14 16:51:35', '2018-06-14 16:51:35'),
(67, 'macaroni', '2018-06-14 16:55:11', '2018-06-14 16:55:11'),
(68, 'zwarte bonen', '2018-06-14 16:56:08', '2018-06-14 16:56:08'),
(69, 'rode ui', '2018-06-14 16:56:42', '2018-06-14 16:56:42'),
(70, 'bosui', '2018-06-14 16:57:03', '2018-06-14 16:57:03'),
(71, 'rucola', '2018-06-14 17:00:32', '2018-06-14 17:00:32'),
(72, 'geroosterde paprika\'s (pot)', '2018-06-14 17:00:58', '2018-06-14 17:00:58'),
(73, 'feta', '2018-06-14 17:01:11', '2018-06-14 17:01:11'),
(74, 'pijnboompitjes', '2018-06-14 17:01:56', '2018-06-14 17:01:56'),
(75, 'pitabroodjes', '2018-06-14 17:04:54', '2018-06-14 17:04:54'),
(76, 'aubergine', '2018-06-14 17:05:12', '2018-06-14 17:05:12'),
(77, 'groene pesto', '2018-06-14 17:06:18', '2018-06-14 17:06:18'),
(78, 'licht meergranen triangel', '2018-06-14 17:10:46', '2018-06-14 17:10:46'),
(79, 'gerookte kipfilet', '2018-06-14 17:11:08', '2018-06-14 17:11:08'),
(80, 'mayonaise', '2018-06-14 17:11:56', '2018-06-14 17:11:56'),
(81, 'bakpoeder', '2018-06-14 17:18:08', '2018-06-14 17:18:08'),
(82, 'zelfrijzend bakmeel', '2018-06-14 17:19:21', '2018-06-14 17:19:21'),
(83, 'lichtbruine basterdsuiker', '2018-06-14 17:19:41', '2018-06-14 17:19:41'),
(84, 'mascarpone', '2018-06-14 17:22:30', '2018-06-14 17:22:30'),
(85, 'frambozen', '2018-06-14 17:22:56', '2018-06-14 17:22:56'),
(86, 'blauwe bessen', '2018-06-14 17:23:09', '2018-06-14 17:23:09'),
(87, 'lange vingers', '2018-06-14 17:24:13', '2018-06-14 17:24:13'),
(88, 'kaneel', '2018-06-14 17:24:25', '2018-06-14 17:24:25'),
(89, 'aardbeien limonade', '2018-06-14 17:25:26', '2018-06-14 17:25:26'),
(90, 'gist', '2018-06-14 17:29:56', '2018-06-14 17:29:56'),
(91, 'banaan', '2018-06-14 17:39:10', '2018-06-14 17:39:10'),
(92, 'yoghurt', '2018-06-14 17:39:28', '2018-06-14 17:39:28'),
(93, 'frambozen (bevroren)', '2018-06-14 17:39:54', '2018-06-14 17:39:54'),
(94, 'bananen (rijpe)', '2018-06-14 17:45:44', '2018-06-14 17:45:44'),
(95, 'havermout vlokken', '2018-06-14 17:46:35', '2018-06-14 17:46:35'),
(96, 'koek- en speculaaskruiden', '2018-06-14 17:47:03', '2018-06-14 17:47:03'),
(97, 'wortel (geraspte)', '2018-06-14 17:47:50', '2018-06-14 17:47:50'),
(98, 'nootjes', '2018-06-14 17:48:07', '2018-06-14 17:48:07');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `recipes`
--

CREATE TABLE `recipes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `sort` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `recipes`
--

INSERT INTO `recipes` (`id`, `name`, `desc`, `sort`, `time`, `number`, `created_at`, `updated_at`) VALUES
(1, 'Ontbijtcrumble met appelcompote & pecannoten', 'Met een warm ontbijtje word je toch net even wat lekkerder wakker. En met deze crumble helemaal!', 'ontbijt', '5 minuten bereiden, 10 minuten oventijd', 2, '2018-06-07 14:36:25', '2018-06-11 13:53:04'),
(2, 'Lunchwrap met aardbeien, avocado en mango', 'Volrijp fruit, friszure limoen, koriander en een pittig pepertje. Een explosie van kleur en smaak.', 'lunch', '15 minuten bereiden', 4, '2018-06-07 17:14:41', '2018-06-15 11:35:53'),
(3, 'Quiche met geitenkaas', 'Van zo\'n quiche op de buffettafel kan iedereen een puntje pakken.', 'bijgerecht', '30 minuten bereiden, 1 uur wachten, 55 minuten oventijd', 8, '2018-06-08 11:46:09', '2018-06-11 14:15:31'),
(4, 'Chocolade taart van chocolade mousse', 'Pure verwennerij voor je gasten maar natuurlijk ook voor jou!', 'gebak', '20 - 30 minuten, 2-3 uur wachttijd', 12, '2018-06-11 14:56:07', '2018-06-11 15:05:11'),
(5, 'Chili Con Carne', 'Deze chili con carne is ook lekker om met brood te eten, maar kan ook goed met rijst.', 'hoofdgerecht', '40 minuten bereiden', 4, '2018-06-11 15:08:33', '2018-06-11 15:52:36'),
(6, 'Monchou taart met kersen', 'Deze monchou taart is zo goddelijk lekker!!!!!', 'gebak', '40 minuten bereiden, 2-3 uur wachttijd', 12, '2018-06-14 16:41:37', '2018-06-14 16:45:13'),
(7, 'Maaltijdsalade met zalm', 'Deze lekkere maaltijdsalade met zalm en krieltjes staat in ongeveer 35 minuten op tafel. Ideaal voor een doordeweekse dag, als je niet al te lang in de keuken wilt staan.', 'salade', '10 minuten bereiden, 25 minuten oventijd', 2, '2018-06-14 16:48:18', '2018-06-14 16:52:19'),
(8, 'Mexicaanse macaroni salade', 'Macaroni is dan misschien niet helemaal Mexicaans, maar met de rest van de ingrediënten krijg je toch even het idee dat je in Mexico zit! Met hopelijk veel lekker zomers weer voor de boeg is deze Mexicaanse macaroni salade een ideaal recept voor bijvoorbeeld bij een barbecue. Ben je nou niet zo gek op zwarte bonen dan kun je deze ook vervangen door kidneybonen.', 'salade', '20 minuten', 2, '2018-06-14 16:54:54', '2018-06-14 16:57:51'),
(9, 'Courgette carpaccio met feta', 'Voorgerechten zullen er waarschijnlijk bij een doorsnee avondmaaltijd gauw bij inschieten, maar voorgerechten zijn niet alleen voorbehouden aan restaurants. Zo is het bijvoorbeeld leuk om visite met een gerechtje vooraf te verrassen of om die doorsnee avondmaaltijd net iets minder doorsnee te maken. Vandaag laten we je zien hoe je deze super lekkere courgette carpaccio met feta maakt.', 'salade', '10 minuten', 2, '2018-06-14 16:59:55', '2018-06-15 12:15:01'),
(10, 'Groenten tosti', 'Deze pitabroodjes gevuld met aubergine, tomaat en ui, doen, met het toevoegen van wat pesto en Italiaanse kruiden, Italiaans aan. Ben je de caprese beu? Probeer dan zeker eens deze groenten tosti, super lekker en zo simpel.', 'lunch', '10 minuten bereiden, 5 minuten oventijd', 4, '2018-06-14 17:04:38', '2018-06-14 17:04:38'),
(11, 'Sandwich met gerookte kip en mango', 'Weer eens wat anders voor de lunch? Wat dacht je van deze sandwich met gerookte kip en mango. Super simpel om te maken, maar lekker voor de afwisseling.', 'lunch', '10-15 minuten', 2, '2018-06-14 17:10:19', '2018-06-14 17:10:19'),
(12, 'Sticky toffee cake', 'Sticky toffee cake, is in mijn ogen – mits je dol bent op toffee en karamel, iets wat iedereen geprobeerd moet hebben. Stel je een luchtige cake voor, doordrenkt met een romige toffeesaus, dat is namelijk precies wat het is. Nog niet overtuigd? Laat de beelden voor zich spreken.', 'dessert', '20 minuten bereiden, 60 minuten oventijd', 10, '2018-06-14 17:17:35', '2018-06-14 17:17:35'),
(13, 'Tiramisu met rood fruit', 'Krijg je binnenkort eters of de vloer en wil je een lekker toetje maken? Deze tiramisu met rood fruit is ontzettend lekker en heel erg simpel om te maken.', 'dessert', '20 minuten bereiden', 6, '2018-06-14 17:22:17', '2018-06-14 17:22:17'),
(14, 'Brioche', 'Brioche is een zoet, zacht, witbrood dat van origine uit Frankrijk komt. Door zijn lichte, luchtige textuur heeft een brioche eigenlijk meer weg van een cake, dan van een brood. Het recept verschilt met dat van een gewoon brood in het extra gebruik van suiker, eieren en roomboter.\r\nEet brioche met wat boter en jam of suiker en kaneel, of gebruik het als basis voor een uitgebreider recept. Zo kun je van brioche ook heerlijke wentelteefjes bakken, of een zoete broodpudding maken.', 'ontbijt', '20 minuten bereiden, 90 minuten reizen, 25 minuten oventijd', 2, '2018-06-14 17:28:13', '2018-06-14 17:31:15'),
(15, 'Smootie bowl met banaan', 'Zin om eens een ander ontbijtje te maken? Wat dacht je van deze smoothie bowl? Super lekker en verfrissend in de zomer en zo klaar! Daarnaast heb je maar weinig ingrediënten nodig.', 'ontbijt', '10-15 minuten bereiden', 1, '2018-06-14 17:38:58', '2018-06-14 18:39:09'),
(16, 'Carrot cake bananenbrood', 'Ik had een paar rijpe bananen liggen waar ik iets mee moest doen. Meestal maak ik een bananenbrood of banaan-ei pannekoekjes ermee. Uiteindelijk heb ik een carrot cake bananenbrood gemaakt met de rijpe bananen die ik nog had liggen. Het recept was verrassend lekker en zeker voor herhaling vatbaar!', 'gebak', '15 minuten, 45 minuten oventijd', 8, '2018-06-14 17:45:22', '2018-06-14 17:49:54');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'A Administrators account', '2018-06-07 13:17:36', '2018-06-07 13:17:36'),
(2, 'user', 'A User account', '2018-06-07 13:17:36', '2018-06-07 13:17:36');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `role_user`
--

CREATE TABLE `role_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `role_user`
--

INSERT INTO `role_user` (`id`, `role_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-06-07 13:17:36', '2018-06-07 13:17:36'),
(2, 2, 2, '2018-06-07 17:08:14', '2018-06-07 17:08:14'),
(3, 2, 3, '2018-06-08 10:26:35', '2018-06-08 10:26:35');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `steps`
--

CREATE TABLE `steps` (
  `id` int(10) UNSIGNED NOT NULL,
  `recipe_id` int(11) NOT NULL,
  `desc` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `steps`
--

INSERT INTO `steps` (`id`, `recipe_id`, `desc`, `created_at`, `updated_at`) VALUES
(1, 1, 'Verwarm de oven voor op 180 °C. Verdeel de compote over de kommen. Hak de pecannoten grof en meng met de speltvlokken en kardemom. Bestrooi de appelcompote met het notenspeltmengsel en zet ca. 10 min. in de oven.', '2018-06-11 09:00:39', '2018-06-11 09:00:39'),
(2, 1, 'Combinatietip:\r\nLekker met thee met kaneel en steranijs. Breng hiervoor (voor 2 personen) 250 ml halfvolle melk met 1 kaneelstokje en 2 steranijsjes tegen de kook aan en laat 5 min. trekken. Verwijder de kaneel en de steranijs. Zet 250 ml sterke ontbijtthee en meng deze met de gekruide melk. Voeg eventueel naar smaak honing toe. (Het recept is met deze thee niet meer lactosevrij.)', '2018-06-11 09:00:58', '2018-06-11 09:00:58'),
(3, 2, 'Rasp de groene schil van de limoen en pers de vrucht uit. Meng de helft van het sap met het rasp, de zuivelspread en de honing. Verwijder de kroontjes van de aardbeien en halveer de vruchten.', '2018-06-11 14:02:11', '2018-06-11 14:02:11'),
(4, 2, 'Snijd de avocado’s in de lengte doormidden, verwijder de pit en schep het vruchtvlees met een lepel uit de schil. Snijd het vruchtvlees in blokjes van 1 x 1 cm. Besprenkel met de rest van het limoensap. Hak de cashewnoten grof. Schil de mango en snijd het vruchtvlees in blokjes. Meng met de aardbeien, avocado en cashewnoten.', '2018-06-11 14:02:20', '2018-06-11 14:02:20'),
(5, 2, 'Snijd het steeltje van de rode peper. Verwijder met een scherp mesje de zaadlijsten. Snijd het vruchtvlees fijn. Snijd de koriander grof. Besmeer de tortillawraps met het zuivelspreadmengsel en beleg met de fruit-notensalade. Bestrooi met de koriander en rode peper.', '2018-06-11 14:02:31', '2018-06-11 14:02:31'),
(6, 3, 'Verwarm de oven voor op 180 °C. Hak de noten grof. Verhit een koekenpan zonder olie of boter en rooster de noten 3 min. Snijd ondertussen de sjalotten in dunne ringen. Schep de sjalotten om met de noten en haal van het vuur. Halveer de dadels, verwijder de pit, snijd het vruchtvlees in parten en schep door de noten.', '2018-06-11 14:08:04', '2018-06-11 14:08:04'),
(7, 3, 'Schil de peren en snijd in de lengte in parten. Klop de eieren los met de slagroom. Rasp de geitenkaas. Snijd de naaldjes rozemarijn fijn. Schep de geitenkaas, rozemarijn, peper en eventueel zout door het eiermengsel. Vet de springvorm in.', '2018-06-11 14:08:15', '2018-06-11 14:08:15'),
(8, 3, 'Bekleed de springvorm met het bladerdeeg en verdeel het notenmengsel over de bodem van de springvorm. Leg de peer erop en verdeel het eimengsel erover.', '2018-06-11 14:08:23', '2018-06-11 14:08:23'),
(9, 3, 'Bak de quiche ca. 55 min. in het midden van de oven. Dek af met aluminiumfolie als hij te donker wordt. Neem uit de oven en laat in de vorm op een rooster in 1 uur afkoelen tot kamertemperatuur.', '2018-06-11 14:08:32', '2018-06-11 14:08:32'),
(10, 3, 'Bereidingstip:\r\nJe kunt de quiche 2 dagen van tevoren bereiden. Bewaar afgedekt in de koelkast. Verwarm voor het serveren 30 min. afgedekt met aluminiumfolie in de oven op 170 °C, of serveer hem op kamertemperatuur.', '2018-06-11 14:08:46', '2018-06-11 14:08:46'),
(11, 4, 'Doe de koekjes in een plastic zakje en rol er met een deegrol over zodat fijne kruimels ontstaan, of verkruimel ze in de foodprocessor. Smelt de boter en roer het koek kruimels erdoor. Verdeel het mengsel over de bodem van een met bakpapier beklede springvorm (20 cm) en druk het met de bolle kant van een lepel aan. Laat de bodem in de koelkast opstijven.', '2018-06-11 14:59:01', '2018-06-11 14:59:01'),
(12, 4, 'Breek de witte en de pure chocolade in stukjes boven aparte pannetjes. Zet de pannetjes elk in een iets grotere pan met kokend water en laat de chocolade al roerende (au bain-marie) smelten. Week de gelatine 5 minuten in koud water. Breng de melk aan de kook en los er, van het vuur af , de goed uitgeknepen gelatine in op. Roer de helft van het gelatine mengsel door de gesmolten witte chocolade en de rest door de gesmolten pure chocolade. Laat beide chocolade mengsels afkoelen, maar niet helemaal opstijven.', '2018-06-11 14:59:29', '2018-06-11 14:59:29'),
(13, 4, 'Klop de slagroom stijven spatel de helft door het witte chocolademengsel en de rest door het pure chocolademengsel. Houd 4 eetlepels van de witte mousse apart en doe de rest over in de vorm. Verdeel de bruine mousse erover. Lepel de achter gebleven witte mousse met een theelepeltje over de taart en roer met een vork of satestokje door de bovenste laag van de taart, zodat een marmer effect ontstaat. Laat de mousse in ? 2-3 uur in de koelkast opstijven.', '2018-06-11 15:00:02', '2018-06-11 15:00:02'),
(14, 4, 'Halveer intussen de aardbeien. Haal de spring vormrand voorzichtig los van de taart en verwijder hem. Haal de bodemplaat voorzichtig los, zet de taart op een bord en garneer met de aardbeien. Bestuif alleen de aardbeien met poedersuiker', '2018-06-11 15:00:19', '2018-06-11 15:00:19'),
(15, 5, 'Fruit ui, knoflook en peper in een diepe pan in olijfolie 2 minuten. Voeg de paprika toe en bak 2 minuten mee. Voeg gehakt, cayennepeper en gemalen komijn toe en bak het gehakt rul in 6 minuten.\r\nVoeg de tomatenblokjes, tomatenpuree en bouillonblokje toe en laat 20 minuutjes inkoken.', '2018-06-11 15:51:55', '2018-06-11 15:51:55'),
(16, 5, 'Voeg de kidneybonen en mais toe en verwarm 5 minuten mee. Serveer met de tortilla chips.', '2018-06-11 15:52:26', '2018-06-11 15:52:26'),
(17, 6, 'Eerst ga je de koekjes verkruimelen. Het is het makkelijkst om dit in een theedoek te doen die je dichtvouwt, want in plastic zakken ontstaan vaak gaatjes (bij mij althans). Dan kun je fijn al je agressie eruit gooien door erop te slaan met een deegroller. Zorg ervoor dat het écht kruimels worden. Je bent me dankbaar als je de taart moet gaan snijden, trust me.', '2018-06-14 16:44:06', '2018-06-14 16:44:06'),
(18, 6, 'Dan doe je bakpapier op de bodem van je springvorm en smelt je de boter langzaam. Als dat gebeurd is, doe je de koekkruimels erbij en meng je dit goed. Het bodemmengsel mag in de springvorm en dat druk je stevig aan. Zet ‘m even in de koelkast terwijl je met het monchoumengsel aan de slag gaat.', '2018-06-14 16:44:13', '2018-06-14 16:44:13'),
(19, 6, 'Doe de slagroom met de vanillesuiker in een kom en klop de slagroom met behulp van de mixer. Niet zo hard dat je er boter van maakt, want dan moet je opnieuw beginnen. Op het laatst de monchou erbij mixen totdat de klontjes verdwenen zijn, en dan kan het mengsel in de springvorm.', '2018-06-14 16:44:51', '2018-06-14 16:44:51'),
(20, 6, 'Laat de taart een paar uur opstijven in de koelkast en doe er net voor het serveren het vlaaifruit op. Daarna ga je met een mes langs de rand en kun je de springvorm voorzichtig verwijderen en ziet je taart er ongeveer zo uit.', '2018-06-14 16:45:00', '2018-06-14 16:45:00'),
(21, 7, 'Verwarm de oven voor op 200 graden. Was de krieltjes en snijd ze in stukjes. Snijd de courgette in plakjes of halve plakjes. Doe de krieltjes, samen met een scheutje olijfolie, snufje zout en peper en de Italiaanse kruiden in een ovenschaal. Meng door elkaar en zet circa 10 minuten in de oven.', '2018-06-14 16:51:44', '2018-06-14 16:51:44'),
(22, 7, 'Voeg daarna de courgette toe, meng nogmaals door elkaar en zet nog 15 minuten in de oven. Ondertussen snijd je de tomaten in stukjes en kook je de eieren. Ik heb zelf voor dit recept warmgerookte zalm gebruikt (erg lekker!) maar je kunt ook kiezen voor een moot zalm die je in de pan bakt of ‘gewone’ gerookte zalm. Meng in een grote kom de gerookte zalm, krieltjes, courgette, tomaten, gekookte eieren en een snufje zout en peper. Voeg de honing en mosterd toe en naar smaak nog een beetje (olijf)olie. En klaar is je maaltijdsalade met zalm!', '2018-06-14 16:52:02', '2018-06-14 16:52:02'),
(23, 7, 'Tip: ook lekker met tonijn of gerookte kip in plaats van zalm.', '2018-06-14 16:52:10', '2018-06-14 16:52:10'),
(24, 8, 'Begin met het opzetten van een pan water en kook de macaroni volgens de aanwijzingen op de verpakking. Ondertussen snijd je de tomaten en uitjes in kleine blokjes. Snijd de avocado pas op laatste moment zodat hij niet helemaal bruin is als de salade klaar is om te serveren.', '2018-06-14 16:57:12', '2018-06-14 16:57:12'),
(25, 8, 'Meng in een schaaltje de Griekse yoghurt met mayo, een klein beetje citroensap, een teentje knoflook en de kruiden.', '2018-06-14 16:57:23', '2018-06-14 16:57:23'),
(26, 8, 'Als de macaroni bijna klaar is snijd je de avocado in blokjes. Dan is het slechts nog een kwestie van macaroni afgieten, en alle ingrediënten samen met de dressing mengen met de macaroni. Zelf hebben we de macaroni eerst even afgespoeld met koud water. (zoals je ziet was ik vergeten om de mais door de salade te mengen, maar die gaat er uiteraard ook doorheen)', '2018-06-14 16:57:30', '2018-06-14 16:57:30'),
(27, 8, 'Tip: Ben je nou een echte vleesliefhebber? Doe er dan wat kip van de barbecue doorheen!', '2018-06-14 16:57:44', '2018-06-14 16:57:44'),
(28, 9, 'Begin met het schaven van de courgette. Gebruik hiervoor simpelweg een kaasschaaf. Giet een scheutje olie in een pan en bak de plakjes courgette met een snufje zout en peper een paar minuten aan. Verdeel de plakjes courgette nu over twee borden. Snijd de geroosterde paprika in kleine blokjes en doe hetzelfde met de feta. Zet ondertussen een klein koekenpannetje op het vuur zonder olie, voor de pijnboompitjes in. Bak ook de pijnboompitten kort aan, totdat ze bruin kleuren. Verdeel tot slot alle ingrediënten over de courgette.', '2018-06-14 17:02:37', '2018-06-14 17:02:37'),
(29, 9, 'Tip: vleesliefhebbers kun je bijvoorbeeld wat krokant gebakken spek aan dit gerecht toevoegen.', '2018-06-14 17:02:50', '2018-06-14 17:02:50'),
(30, 10, 'Snijd de ui en de aubergine fijn. Wij kozen ervoor de aubergine in plakken te snijden. Bak beide even kort aan in de pan met de Italiaanse kruiden en een snuf peper en zout.', '2018-06-14 17:07:09', '2018-06-14 17:07:09'),
(31, 10, 'Verwarm ondertussen de oven voor op 180 graden. Snijd de pitabroodjes open en besmeer ze met een laagje pesto. Vul de pitabroodjes met aubergine, ui en tomaat. Schuif de pitabroodjes tot slot voor 5 minuten in de oven of bak de groenten tosti 5 minuten in je tosti-apparaat.', '2018-06-14 17:07:27', '2018-06-14 17:07:27'),
(32, 10, 'Tip: voeg eens wat plakjes mozzarella toe aan de vulling van de pitabroodjes.\r\nTip: vervang de pesto voor hummus, ook erg lekker!', '2018-06-14 17:07:47', '2018-06-14 17:07:47'),
(33, 11, 'Snijd de gerookte kipfilet in stukjes. Schil de mango en verwijder de pit. Snijd de mango in plakjes. Besmeer de broodjes met mayo. Leg hier de sla, gerookte kip en mango op. Verdeel een beetje honing en een snufje zout en peper over het geheel.', '2018-06-14 17:13:06', '2018-06-14 17:13:06'),
(34, 12, 'Verwarm voor je begint de oven vast voor op 180 graden. Snijd de dadels in kleine stukjes en zet deze vervolgens op met 200 ml water. Haal de pan van het vuur wanneer het water kort heeft gekookt. Roer hier nu een theelepel bakpoeder doorheen en laat het mengsel afkoelen.', '2018-06-14 17:20:04', '2018-06-14 17:20:04'),
(35, 12, 'Klop 100 gram boter met de suiker en het vanillesuiker luchtig. Klop hier vervolgens een voor een de eieren door. Zeef ook het zelfrijzend bakmeel erboven en roer dit er goed doorheen. Door het bakmeel door een fijne zeef te halen krijg je luchtiger meel. Tip: luchtiger meel zorgt voor luchtiger gebak. Pureer de dadels samen met het weekwater en meng ook dit door het beslag. Schenk het beslag in een gevet bakblik en bak de cake in ongeveer 50 minuten gaar.', '2018-06-14 17:20:11', '2018-06-14 17:20:11'),
(36, 12, 'Verwarm ondertussen al roerend de basterdsuiker met de slagroom en de rest van de boter.', '2018-06-14 17:20:20', '2018-06-14 17:20:20'),
(37, 12, 'Laat de saus kort koken wanneer de suiker volledig gesmolten is.', '2018-06-14 17:20:29', '2018-06-14 17:20:29'),
(38, 12, 'Schenk een groot deel van de saus over de warme cake. Zet deze nogmaals voor 10 minuten terug in de oven, tot de cake de saus heeft geabsorbeerd.', '2018-06-14 17:20:38', '2018-06-14 17:20:38'),
(39, 12, 'Laat de sticky toffee cake goed afkoelen voor je er stukken van snijd. Serveer een plak sticky toffee op een bord en giet nogmaals een beetje toffeesaus er overheen.', '2018-06-14 17:20:58', '2018-06-14 17:20:58'),
(40, 13, 'Doe de slagroom in een kom en klop deze samen met de suiker luchtig. Roer de mascarpone met een mixer wat luchtiger en schep deze vervolgens door de slagroom, samen met de kaneel. Doe de aardbeien limonade in een kommetje.', '2018-06-14 17:25:48', '2018-06-14 17:25:48'),
(41, 13, 'Verdeel een beetje van het mascarpone mengsel over de bodem van een rechthoekige schaal. Mijn schaal is 21 cm lang. Doop een lange vinger even kort in de aardbeienlimonade en leg hem daarna in de schaal, op het mascarpone-mengsel. Bedek de hele bodem van de schaal met lange vingers, die je even in de limonade hebt gedoopt.', '2018-06-14 17:26:06', '2018-06-14 17:26:06'),
(42, 13, 'Daarna een laagje fruit, een laagje mascarpone, laagje lange vingers enz. Eindig met een laagje fruit. Verdeel eventueel nog wat geschaafde amandelen over de tiramisu of wat geraspte chocolade. Bewaar de tiramisu in de koelkast totdat je hem gaat serveren.', '2018-06-14 17:26:18', '2018-06-14 17:26:18'),
(43, 14, 'Zorg voor lauwwarme melk. Mocht je de melk net uit de koelkast halen, verwarm het dan even kort in de magnetron. Voeg de gist en 3 tl suikers toe aan de melk. Roer dit door en zet het kort weg. Doe vervolgens de bloem, de resterende suiker, het zout en het ei in een mengkom. Heb je een keukenmachine, maak hier dan zeker gebruik van, zo niet, dan is het deeg ook goed met de hand te kneden. Voeg nu ook de melk toe en kneed hier een soepel deeg van.', '2018-06-14 17:30:21', '2018-06-14 17:30:21'),
(44, 14, 'Voeg vervolgens ook beetje bij beetje de zachte boter toe. Het kan even duren voordat je een samenhangend deeg krijgt, maar hoe langer je kneed, hoe beter het wordt. Plakkerig blijft het wel.', '2018-06-14 17:30:31', '2018-06-14 17:30:31'),
(45, 14, 'Stort het deeg op een met zonnebloemolie ingevet werkblad en kneed het deeg kort na met de hand. Wanneer het deeg een beetje lijkt op kauwgom als je het uit elkaar trekt, dan is het goed. Vet een kom in met olie en laat het deeg hier ongeveer 45 minuten in rijzen onder een natte theedoek. Het deeg zal zich in volume verdubbelen.', '2018-06-14 17:30:41', '2018-06-14 17:30:41'),
(46, 14, 'Kneed het deeg na de eerste rijs nogmaals kort en verdeel het in 10 gelijke delen. Vorm hier kleine balletjes van. Leg de bollen in een ingevet cakeblik tegen elkaar aan. Dek het cakeblik af met wederom een natte doek en laat het deeg nogmaals 45 minuten rijzen.', '2018-06-14 17:30:57', '2018-06-14 17:30:57'),
(47, 14, 'Verwarm halverwege de laatste rijs de oven voor op 180 graden. Bak de brioche in zo’n 25 minuten bruin. Om de brioche extra te laten glazen kan je hem bestrijken met wat melk voordat je hem de oven in schuift.', '2018-06-14 17:31:01', '2018-06-14 17:31:01'),
(48, 15, 'Doe de banaan, yoghurt en bevroren frambozen in een blender. Mix alles goed door elkaar totdat alles goed gepureerd is. Je kunt eventueel een beetje melk toevoegen als je het geheel te dik vindt. Als alles goed gepureerd is, kun je de smoothie in een kom scheppen/gieten. Versier de smoothie bowl met plakjes banaan, wat geraspte kokos en wat stukjes aardbei. Je kunt uiteraard ook kiezen voor andere toppings zoals fijngehakt nootjes of ander fruit.', '2018-06-14 17:40:15', '2018-06-14 17:40:15'),
(49, 15, 'Toppings: halve banaan, geraspte kokos en een paar aardbeien.', '2018-06-14 17:40:23', '2018-06-14 17:40:23'),
(50, 16, 'Verwarm de oven voor op 200 graden. Prak de bananen in een kom tot puree. Voeg vervolgens de eieren toe en meng dit door elkaar.', '2018-06-14 17:48:45', '2018-06-14 17:48:45'),
(51, 16, 'Daarna kan het zelfrijzend bakmeel, de havermout vlokken, koek- en speculaaskruiden en kaneel erbij. Vervolgens rasp je de wortels en voeg je die, net zoals de melk,  toe aan het beslag.', '2018-06-14 17:49:00', '2018-06-14 17:49:00'),
(52, 16, 'Vet een cakevorm in en giet het beslag in de vorm. Zet het bananenbrood voor circa 45 minuten in de oven. Meng in een bakje de monchou met de poedersuiker.', '2018-06-14 17:49:18', '2018-06-14 17:49:18'),
(53, 16, 'Laat het bananenbrood helemaal afkoelen en smeer het monchou mengsel over het bananenbrood. Maak af met wat fijngehakte nootjes.', '2018-06-14 17:49:27', '2018-06-14 17:49:27');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `units` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `units`
--

INSERT INTO `units` (`id`, `units`, `created_at`, `updated_at`) VALUES
(1, 'cl', '2018-06-11 09:01:22', '2018-06-11 09:01:22'),
(2, 'ml', '2018-06-11 09:01:27', '2018-06-11 09:01:27'),
(3, 'kg', '2018-06-11 09:01:36', '2018-06-11 09:01:36'),
(4, 'liter', '2018-06-11 09:01:43', '2018-06-11 09:01:43'),
(5, 'gram', '2018-06-11 13:51:31', '2018-06-11 13:51:31'),
(6, 'stuks', '2018-06-11 13:51:44', '2018-06-11 13:51:44'),
(7, 'stuk', '2018-06-11 13:51:50', '2018-06-11 13:51:50'),
(8, 'bus', '2018-06-11 13:52:04', '2018-06-11 13:52:04'),
(9, 'rol', '2018-06-11 13:52:11', '2018-06-11 13:52:11'),
(10, 'el', '2018-06-11 13:53:54', '2018-06-11 13:53:54'),
(11, 'tl', '2018-06-11 13:53:58', '2018-06-11 13:53:58'),
(12, 'takjes', '2018-06-11 14:05:23', '2018-06-11 14:05:23'),
(13, 'blaadjes', '2018-06-11 14:16:49', '2018-06-11 14:16:49'),
(14, 'dl', '2018-06-11 14:54:08', '2018-06-11 14:54:08'),
(15, 'zak', '2018-06-11 15:09:24', '2018-06-11 15:09:24'),
(16, 'blikje', '2018-06-11 15:09:30', '2018-06-11 15:09:30'),
(17, 'blokje', '2018-06-11 15:09:37', '2018-06-11 15:09:37'),
(18, 'snuf', '2018-06-11 15:09:44', '2018-06-11 15:09:44'),
(19, 'teentje', '2018-06-11 15:10:04', '2018-06-11 15:10:04'),
(20, 'zakjes', '2018-06-14 16:42:58', '2018-06-14 16:42:58'),
(21, 'handje', '2018-06-14 17:01:30', '2018-06-14 17:01:30'),
(22, 'pak', '2018-06-14 17:23:51', '2018-06-14 17:23:51'),
(23, 'glas', '2018-06-14 17:24:56', '2018-06-14 17:24:56');

-- --------------------------------------------------------

--
-- Tabelstructuur voor tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Gegevens worden geëxporteerd voor tabel `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Daphne de Groot', 'daphne.degroot86@gmail.com', '$2y$10$Rbymx7OFWyFaF4EX4Z90fuLRnquT3Gh76SwYS86r70Sf3XmSlI85q', '2iRHF6mALpOwgT3SmuLi9AqTePWwANnjozkAlYCGxl1VrriYzAQMJ5ES4JVj', '2018-06-07 13:17:36', '2018-06-07 13:17:36'),
(2, 'Jean Verhasselt', 'jean.verhasselt@veconengineering.com', '$2y$10$..pRvYz1ynuMSX58Oaeg5.J9Vxpt/GahPnuN/bezpClqJTVnwi2fi', 'HbYwd4y2fAxtivjTFq6D2EeJMx8Cn0XhHYLZFlhuutBUw2RCQI88hX58BZbT', '2018-06-07 17:08:14', '2018-06-07 17:08:14'),
(3, 'Daphne Verhasselt', 'd.degroot1986@gmail.com', '$2y$10$mqczfS79PBvWOQabrzYBSeYm/VBw.ctNYcfboGq/cxEy1gNY7LWQi', 'IOyr5fT4ChyJviK4PLuEC5lq9BoxRfpXDPPaEsnHnRzew0mFe6LutOln67UM', '2018-06-08 10:26:35', '2018-06-08 10:26:35');

--
-- Indexen voor geëxporteerde tabellen
--

--
-- Indexen voor tabel `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `ingredients`
--
ALTER TABLE `ingredients`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `ingredient_product`
--
ALTER TABLE `ingredient_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `ingredient_unit`
--
ALTER TABLE `ingredient_unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexen voor tabel `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `recipes`
--
ALTER TABLE `recipes`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `steps`
--
ALTER TABLE `steps`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`);

--
-- Indexen voor tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT voor geëxporteerde tabellen
--

--
-- AUTO_INCREMENT voor een tabel `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT voor een tabel `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT voor een tabel `ingredients`
--
ALTER TABLE `ingredients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT voor een tabel `ingredient_product`
--
ALTER TABLE `ingredient_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;

--
-- AUTO_INCREMENT voor een tabel `ingredient_unit`
--
ALTER TABLE `ingredient_unit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=138;

--
-- AUTO_INCREMENT voor een tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT voor een tabel `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT voor een tabel `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT voor een tabel `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;

--
-- AUTO_INCREMENT voor een tabel `recipes`
--
ALTER TABLE `recipes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT voor een tabel `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT voor een tabel `role_user`
--
ALTER TABLE `role_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT voor een tabel `steps`
--
ALTER TABLE `steps`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT voor een tabel `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT voor een tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

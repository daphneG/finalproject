<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role_administrator = new Role;

        $role_administrator->name = 'admin';
        $role_administrator->description = 'A Administrators account';
        $role_administrator->save();

        $role_user = new Role;
        $role_user->name = 'user';
        $role_user->description = 'A User account';
        $role_user->save();
    }
}

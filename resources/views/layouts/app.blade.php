@include('inc.header')

    <div class="container-fluid">

        <main>
            @yield('content')
        </main>
    </div>

@include('inc.footer')


@include('inc.header')

@include('inc.navbarAdmin')

<div class="container-fluid">

    <div class="row">
        @include('inc.sidebar')

        @yield('content')

    </div>

</div>

@include('inc.footer')
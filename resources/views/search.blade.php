@extends('layouts.app')

@include('inc.navbar')

@section('content')

    <div class="col-sm-12 mt-3">
        <h1 class="text-center">Gevonden recept</h1>
        <hr>
        <div class="row">
            @foreach($recipes as $recipe)
                <div class="col-md-8 {{ $recipe->sort }}">
                    <div class="card">
                        <div class="card-body">
                            <img src="/images/{{ isset($recipe->photos->path) ? $recipe->photos->path : ''  }}" alt="image" class="post_img" >
                            <h5 class="card-title" style="min-height: 50px;">{{ $recipe->name }}</h5>
                            <p class="card-text" style="min-height: 70px;">{{ $recipe->desc }}</p>
                            <a href="{{ route('gerechten.show', $recipe->id) }}">Lees meer</a>
                            @if (Auth::check())
                                @foreach($favorites as $favorite)
                                    @if($recipe->id === $favorite->recipe_id)
                                        @php $isset = true; @endphp
                                    @endif
                                @endforeach
                                @if(!isset($isset))
                                    <form action="{{ route('favorites.store', $recipe) }}" method="POST">
                                        @csrf
                                        <button class="btn btn-danger btn-sm" style="float: right;">Add to <i class="far fa-fw fa-heart"></i></button>
                                    </form>
                                @endif
                            @endif
                        </div>
                    </div>
                    <br>
                </div>
            @endforeach
                <div class="col-md-4">
                    <img src="images/fruits.jpg" alt="image" style="width: 100%; height: 80%;">
                </div>
        </div>
    </div>


@endsection
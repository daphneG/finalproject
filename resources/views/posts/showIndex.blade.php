@extends('layouts.app')

@include('inc.navbar')

@section('content')

    <main class="col-sm-12">
        <div class="row">
            <div class="col-sm-12 mt-3">

                {{--Search--}}
                <div class="well">
                    <form action="{{ route('posts.search') }}" method="post">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input name="search" type="text" class="form-control">
                            <span class="form-inline">
                                <button name="submit" class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
                            </span>
                        </div>
                    </form>
                </div><br>
                {{--End Search--}}
                <h1 class="text-center">Welkom op de Blog</h1>
                <hr>
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img class="d-block block-styling" src="/images/hero-recepten.jpg" alt="First slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block block-styling" src="/images/pasta.jpg" alt="Second slide">
                            </div>
                            <div class="carousel-item">
                                <img class="d-block block-styling" src="/images/groentes.jpg" alt="Third slide">
                            </div>
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                <br>
                <br>
                <br>
                <br>


        @foreach($posts as $post)
                <div class="col-md-12">

                    {{--Show Posts--}}
                    <div class="card">

                        <div class="card-body">
                            <img src="/images/{{ isset($post->photos->path) ? $post->photos->path : ''  }}" alt="image" class="post_img" >
                            <h3 class="card-title" style="min-height: 50px;">{{ $post->title }}</h3>
                            <p class="card-text text-muted">Gepost op: {{ $post->created_at }}</p>
                            <p class="card-text" style="min-height: 30px;">{!! strip_tags(str_limit($post->body)) !!}</p>
                            <a href="{{ route('post.detail', $post) }}">Lees meer</a>
                        </div>
                    </div>
                    <br>
                </div>
            @endforeach
            {{--End show Posts--}}
            {{ $posts->links() }}
        </div>
        </div>
    </main>

@endsection
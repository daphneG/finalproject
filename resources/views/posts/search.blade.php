@extends('layouts.app')

@include('inc.navbar')

@section('content')

<main class="col-sm-12 mt-3">
    <h1 class="text-center">Gevonden Blog</h1>
    <hr>
    <div class="row">
        @foreach($posts as $post)
            <div class="col-md-8">
                {{--Show Posts--}}
                <div class="card">
                    <div class="card-body">
                        <img src="/images/{{ isset($post->photos->path) ? $post->photos->path : ''  }}" alt="image" class="post_img" >
                        <h4 class="card-title" style="min-height: 50px;">{{ $post->title }}</h4>
                        <p class="card-text text-muted">Gepost op: {{ $post->created_at }}</p>
                        <p class="card-text" style="min-height: 30px;">{!! strip_tags(str_limit($post->body)) !!}</p>
                        <a href="{{ route('post.detail', $post) }}">Lees meer</a>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="col-md-4">
            <img src="images/healthyFood.jpg" alt="image" style="width: 100%; height: 80%;">
        </div>
    </div>
</main>

@endsection
@extends('layouts.app')

@include('inc.navbar')

@section('content')


    <div class="row no-gutters">
        <div class="card col-md-12 mt-3">

            {{--Show Post--}}
            <div class="card-body">
                <img src="/images/{{ isset($post->photos->path) ? $post->photos->path : ''  }}" alt="image" class="show_img">
                <h3 class="card-title" style="min-height: 40px;">{{ $post->title }}</h3>
                <p class="card-text text-muted">Gepost op: {{ $post->created_at }}</p>
                <p class="card-text" style="min-height: 30px;">{!! $post->body !!}</p>
                <a href="{{ route('posts.showIndex') }}" class="btn btn-outline-info btn-sm">Terug naar Home</a>
            </div>
        </div>
        <br>
        {{--End show Posts--}}

        {{--Show Comment--}}
        <div class="col-md-6 pr-4 mt-3">
            @foreach($post->comments as $comment)
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">{{ $comment->name }}</h5>
                    <p class="card-text text-muted">Gepost op: {{ $comment->created_at }}</p>
                    <p class="card-text">{{ $comment->comment }}</p>
                    @if (Auth::check() && Auth::user()->hasrole('admin'))
                        <form action="{{ route('comments.destroy', [$post->id, $comment->id]) }}" method="post">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <button class="btn btn-outline-danger btn-sm">Delete</button>
                        </form>
                    @endif
                </div>
            </div>
            <br>
            @endforeach
        </div>
        {{--End show Comment--}}

        {{--Create Post--}}
        <div class="col-md-6 mt-3">
            <form action="{{ route('comments.store', $post->id ) }}" method="post">
                {{ csrf_field() }}
                <div class="card">
                    <h6 class="card-header text-muted">Commentaar</h6>
                    <div class="card-body">
                        <div class="form-group">
                            <h6 class="card-title">Naam</h6>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>
                        <div class="form-group">
                            <h6 class="card-title">Email</h6>
                            <input type="text" name="email" id="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <h6 class="card-title">Comment</h6>
                            <textarea type="text" name="comment" class="form-control"></textarea>
                        </div>
                        <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Verzenden</button>
                    </div>
                </div>
            </form>
        </div>
        {{--End create Post--}}
    </div>




@endsection
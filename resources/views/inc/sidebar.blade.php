<div class="col-md-4 col-lg-2 mt-3">
    <div class="card">
        <div class="card-header"><i class="fas fa-utensils"></i> Recepten</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('recipes.index') }}" class="card-link text-muted" style="color: #000">Alle recepten</a></li>
                <li class="list-group-item"><a href="{{ route('products.index') }}" class="card-link text-muted" style="color: #000">Alle product</a></li>
                <li class="list-group-item"><a href="{{ route('units.index') }}" class="card-link text-muted" style="color: #000">Alle eenheden</a></li>
                <li class="list-group-item"><a href="{{ route('recipes.create') }}" class="card-link text-muted" style="color: #000">Nieuw recept</a></li>
                <li class="list-group-item"><a href="{{ route('products.create') }}" class="card-link text-muted" style="color: #000">Nieuw product</a></li>
                <li class="list-group-item"><a href="{{ route('units.create') }}" class="card-link text-muted" style="color: #000">Nieuw eenheid</a></li>
            </ul>
    </div>
    <br>
    <div class="card">
        <div class="card-header"><i class="fab fa-blogger-b"></i> Posts</div>
        <ul class="list-group list-group-flush">
            <li class="list-group-item"><a href="{{ route('posts.index') }}" class="card-link text-muted">Alle posts</a></li>
            <li class="list-group-item"><a href="{{ route('categories.index') }}" class="card-link text-muted">Alle categorie&euml;n</a></li>
            <li class="list-group-item"><a href="{{ route('posts.create') }}" class="card-link text-muted">Nieuwe post</a></li>
            <li class="list-group-item"><a href="{{ route('categories.create') }}" class="card-link text-muted">Nieuwe categorie</a></li>
        </ul>
    </div>
    <br>
    <div class="card">
        <div class="card-header"><i class="fas fa-at"></i> Contacts</div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item"><a href="{{ route('contacts.index') }}" class="card-link text-muted">Alle vragen</a></li>
            </ul>
        </div>
    </div>
{{--@if(session('status'))--}}
    {{--<div class="alert alert-success mb-3">--}}
        {{--{{ session('status') }}--}}
    {{--</div>--}}
{{--@endif--}}

@if(session('success'))
    <div class="alert alert-success mb-3">
        {{ session('success') }}
    </div>
@endif

@if(session('failed'))
    <div class="alert alert-danger mb-3">
        {{ session('failed') }}
    </div>
@endif
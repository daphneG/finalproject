@extends('layouts.app')

@include('inc.navbar')

@section('content')

    <div class="col-md-12 mt-3">
        <h1 class="text-center">Over ons</h1>
        <hr>
        <div class="row">
            <div class="col-md-8">
                <h3>Hoe ben ik begonnen?</h3>
                <p>Ik heb mijn passie voor koken uiteindelijk gevonden toen ik mijn huidige vriend leerde kennen. Hij hield van lekker eten en ik was nog groen achter de oren als het op mijn kookkunsten aan kwam. En natuurlijk, ook ik was bang dat mensen mijn eten niet graag zouden eten. Het tegendeel werd gelukkig bewezen, mijn vriend genoot bij iedere hap van mijn eerste gerecht, waarvoor ik 3 uur in de keuken had gestaan. Vanaf dat moment heb ik mijn passie gevonden voor koken.</p>
                <p>Bakken was heel iets anders, dat was me met de paplepel ingegeven. Mijn vader had de opleiding banketbakker gedaan en mijn moeder die vond het natuurlijk gewoon leuk om met mij in de keuken te staan en te bakken. Vooral op het laatste dat je de gardes kon aflikken.</p>
                <h3>Mijn blunders</h3>
                <p>Ook ik heb er genoeg gehad, van de hele keuken onder de smurrie tot taarten waar ik iets teveel van een ingredi&euml;nt in had gedaan en het zo ontzettend vies smaakte.</p>
                <p>Een recept wel 3x overnieuw maken en dan nog te laten mislukken en niet weten waar je de fout in gaat. Dan is het gewoon beter om het recept aan te passen en er zelf een leuke draai aan te geven en achteraf er hard om te lachen.</p>
                <h3>Mij leren kennen?</h3>
                <h6><strong>Lievelings eten</strong></h6>
                <p>Zuurkoolstamppot, zeker weten mijn favoriet numero uno. Maar lasagne absoluut ook eentje waar je me voor wakker mag maken.</p>
                <h6><strong>Koken of bakken</strong></h6>
                <p>Moeilijke keuze maar ik ga toch voor bakken, daar kan ik me bij ontspannen en dan lekker een vrije dag mee losgaan om zo'n taart of gebakje in elkaar te zetten. </p>
            </div>
            <div class="col-md-4">
                <img src="/images/loveBaking.jpg" style="width: 100%; height: 80%;" alt="image">
            </div>
        </div>
    </div>

@endsection
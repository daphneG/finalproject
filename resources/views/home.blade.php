@extends('layouts.app')

@include('inc.navbarUser')

@section('content')

<div class="col-md-12 mt-3">
    <select name="recipeDropdown" id="recipeDropdown">
        <option>filter recepten</option>
        <option value="ontbijt">Ontbijt</option>
        <option value="lunch">Lunch</option>
        <option value="salade">Salades</option>
        <option value="voorgerecht">Voorgerechten</option>
        <option value="hoofdgerecht">Hoofdgerechten</option>
        <option value="bijgerecht">Bijgerechten</option>
        <option value="dessert">Dessert</option>
        <option value="gebak">Gebak & Taarten</option>
    </select>
    <h2 class="text-center">Mijn eigen kookboek</h2>
    <p class="text-center">Ga naar de homepagina en druk op de knop favorieten om een recept toe te voegen aan je kookboek.</p>

    <div class="row justify-content-center">

        @foreach($favorites as $favorite)
            @php $recipe = $recipes->find($favorite->recipe_id) @endphp

            <div class="col-md-4 col-lg-2 col-xl-3 recepten {{ $recipe->sort }}">
                <div class="card">
                    <img src="/images/{{ isset($recipe->photos->path) ? $recipe->photos->path : ''  }}" alt="image" class="welcome_img" >
                    <div class="card-body">
                        <h5 class="card-title" style="min-height: 50px;">{{ $recipe->name }}</h5>
                        <p class="card-text" style="min-height: 70px;">{{ $recipe->desc }}</p>
                        <a href="{{ route('gerechten.show', $recipe->id) }}">Lees meer</a>
                        @if (Auth::check() && Auth::user()->favorites()->where('recipe_id', $recipe->id) === null )
                            <form action="{{ route('favorites.store', $recipe) }}" method="POST">
                                @csrf
                                <button class="btn btn-danger btn-sm" style="float: right;"><i class="far fa-fw fa-heart"></i> Add to Fav</button>
                            </form>
                        @endif
                        <form action="{{ route('favorites.destroy', $favorite) }}" method="post">
                            {{ csrf_field() }}
                            @method('DELETE')
                            <button class="btn btn-outline-danger btn-sm">Delete</button>
                        </form>
                    </div>
                </div>
                <br>
            </div>
        @endforeach
    </div>
</div>


@endsection

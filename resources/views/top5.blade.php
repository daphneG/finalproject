@extends('layouts.app')

@include('inc.navbar')

@section('content')

    <div class="col-md-12 mt-3">
        <h1 class="text-center">Top 5 beste gerechten van de maand</h1>
        <p class="text-center">Hier kunt u de gerechten vinden die het meest in de smaak zijn gevallen bij onze bezoekers!</p>
        <hr>
        <div class="row no-gutters">

            <div class="col-md-4" style="border: 1px solid #FFFFFF;">
                <div class="card">
                    <h2 class="text-center mt-3">Lunch</h2>
                    @foreach($recipe1 as $recipe)
                        <div class="card-body">
                            <img src="/images/{{ isset($recipe->photos->path) ? $recipe->photos->path : ''  }}" alt="image" class="top5Img">
                            <h5 class="card-title" style="min-height: 45px;">{{ $recipe->name }}</h5>
                            <p class="card-text" style="min-height: 90px;">{{ $recipe->desc }}</p>
                            <p><i class="far fa-clock"></i> {{ $recipe->time }}</p>
                            <p><i class="fas fa-user"></i> {{ $recipe->number }} personen</p>
                            <br>
                            <h5 class="card-title">Ingredi&euml;nten</h5>
                            <ul>
                                @foreach($recipe->ingredients as $ingredient)
                                    <li><p>{{ floatval($ingredient->quantity) }} {{ $ingredient->units->first()['units'] }} {{ $ingredient->products->first()['name'] }}</p></li>
                                @endforeach
                            </ul>
                            <br>
                            <h5 class="card-title">Bereiding</h5>
                            @foreach($recipe->steps as $step)
                                <p class="card-text" style="min-height: 19.3%">{{ $step->desc }}</p>
                            @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
            <div class="col-md-8" style="border: 1px solid #FFFFFF;">
                <div class="card">
                    <h2 class="text-center mt-3">Voorgerecht</h2>
                    @foreach($recipe2 as $recipe)
                        <div class="card-body">
                            <img src="/images/{{ isset($recipe->photos->path) ? $recipe->photos->path : ''  }}" alt="image" class="top5_img">
                            <h5 class="card-title" style="min-height: 45px;">{{ $recipe->name }}</h5>
                            <p class="card-text" style="min-height: 90px;">{{ $recipe->desc }}</p>
                            <a href="{{ route('gerechten.show', $recipe->id) }}">Lees meer</a>
                            @endforeach
                        </div>
                </div>
                    <div class="row no-gutters">
                        <div class="col-md-12" style="border: 1px solid #FFFFFF;">
                            <div class="card">
                                <h2 class="text-center mt-3">Hoofdgerecht</h2>
                                @foreach($recipe3 as $recipe)
                                    <div class="card-body">
                                        <img src="/images/{{ isset($recipe->photos->path) ? $recipe->photos->path : ''  }}" alt="image" class="top5_img">
                                        <h5 class="card-title" style="min-height: 45px;">{{ $recipe->name }}</h5>
                                        <p class="card-text" style="min-height: 90px;">{{ $recipe->desc }}</p>
                                        <a href="{{ route('gerechten.show', $recipe->id) }}">Lees meer</a>
                                        @endforeach
                                    </div>
                            </div>
                        </div>
                    </div>
                <div class="row no-gutters">
                    <div class="col-md-6" style="border: 1px solid #FFFFFF;">
                        <div class="card">
                            <h2 class="text-center mt-3">Gebak</h2>
                            @foreach($recipe4 as $recipe)
                                <div class="card-body">
                                    <img src="/images/{{ isset($recipe->photos->path) ? $recipe->photos->path : ''  }}" alt="image" class="top_5_Img">
                                    <h5 class="card-title" style="min-height: 45px;">{{ $recipe->name }}</h5>
                                    <p class="card-text" style="min-height: 70px;">{{ $recipe->desc }}</p>
                                    <a href="{{ route('gerechten.show', $recipe->id) }}">Lees meer</a>
                                    @endforeach
                                </div>
                        </div>
                    </div>
                    <div class="col-md-6" style="border: 1px solid #FFFFFF;">
                        <div class="card">
                            <h2 class="text-center mt-3">Bijgerecht</h2>
                            @foreach($recipe5 as $recipe)
                                <div class="card-body">
                                    <img src="/images/{{ isset($recipe->photos->path) ? $recipe->photos->path : ''  }}" alt="image" class="top_5_Img">
                                    <h5 class="card-title" style="min-height: 45px;">{{ $recipe->name }}</h5>
                                    <p class="card-text" style="min-height: 70px;">{{ $recipe->desc }}</p>
                                    <a href="{{ route('gerechten.show', $recipe->id) }}">Lees meer</a>
                                    @endforeach
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
@extends('layouts.admin')

@section('content')


    <div class="col-md-10 mt-3">

        {{--Table Contacts--}}
        <div class="card">
            <h6 class="card-header text-muted">Alle Contacten</h6>
            <div class="card-body">
                <table class="table table-sm table-hover">
                    <thead>
                    <tr>
                        <th scope="col" width="30">#</th>
                        <th>Voornaam</th>
                        <th>Achternaam</th>
                        <th>Email</th>
                        <th>Telefoonnummer</th>
                        <th>Vraag/Opmerking</th>
                        <th>Controls</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($contacts as $contact)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $contact->firstName }}</td>
                            <td>{{ $contact->lastName }}</td>
                            <td>{{ $contact->email }}</td>
                            <td>{{ $contact->phoneNumber }}</td>
                            <td>{{ $contact->question }}</td>
                            <td>
                                <form action="{{ route('contacts.destroy', $contact->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{--End table Contacts--}}
    </div>


@endsection
@extends('layouts.app')

@include('inc.navbar')

@section('content')
<div class="page-header">
    <div class="col-md-12 mt-3">
        <h1 class="text-center">Contact</h1>
        <hr>
        <h3 class="text-center">We <i class="far fa-heart"></i> Feedback</h3>
        <p class="text-center">Wij vinden het fijn om te helpen, dus stel die vraag of misschien een aan te raden recept!</p>
        <br>
        <form action="{{ route('contacts.store') }}" method="post" class="row ">
            <div class="col-sm-12 col-md-4 offset-md-2">
                {{--Create Contacts--}}

                {{ csrf_field() }}
                <div class="form-group">
                    <input type="text" name="firstName" class="form-control" placeholder="Voornaam">
                    @if($errors->has('firstName'))
                        <span class="text-danger">Voornaam is verplicht.</span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="text" name="lastName" class="form-control" placeholder="Achternaam">
                    @if($errors->has('lastName'))
                        <span class="text-danger">Achternaam is verplicht.</span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="text" name="email" class="form-control" placeholder="E-mailadres">
                    @if($errors->has('email'))
                        <span class="text-danger">Email is verplicht.</span>
                    @endif
                </div>
                <div class="form-group">
                    <input type="text" name="phoneNumber" class="form-control" placeholder="Telefoonnummer">
                    @if($errors->has('phoneNumber'))
                        <span class="text-danger">Telefoonnummer is verplicht.</span>
                    @endif
                </div>
                <div class="form-group">
                    <textarea name="question" id="question" cols="30" rows="10" class="form-control" placeholder="Vraag/opmerking"></textarea>
                    @if($errors->has('question'))
                        <span class="text-danger">Veld is verplicht.</span>
                    @endif
                </div>
                <div class="form-group">
                    <button type="submit" name="submit" class="btn btn-outline-light btn-sm">Verzenden</button>
                </div>
                {{--End create Contacts--}}
            </div>
            <div class="col-sm-12 col-md-4">
                <p><i class="fas fa-map-marker-alt"></i> Adelbert van Scharnlaan S9 <br>6226 EZ Maastricht</p>

                <p><i class="fas fa-phone"></i> +(0)316 33 20 34 78</p>
                <p><i class="fas fa-envelope"></i> d.degroot1986@gmail.com</p>
            </div>
        </form>
    </div>
</div>

@endsection

@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">

        {{--Begin create Post--}}
        <form action="{{ route('posts.store') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="card">
                <h6 class="card-header text-muted">Nieuwe posts</h6>
                <div class="card-body">
                    <div class="card-title">Post titel</div>
                    <div class="form-group">
                        <input type="text" name="title" class="form-control">
                        @if($errors->has('title'))
                            <span class="text-danger">Titel is verplicht.</span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="file" name="file">
                        @if($errors->has('file'))
                            <span class="text-danger">Foto uploaden is verplicht.</span>
                        @endif
                    </div>

                    {{--Select Category--}}
                    <div class="card-title">Categorie&euml;n</div>
                    <div class="form-group">
                        <select name="category_id" id="category" class="form-control">
                            <option>Please select one</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{ $category->name }}</option>
                        @endforeach
                        </select>
                    </div>
                    {{--End select Category--}}

                    <div class="form-group">
                        <div class="card-title">Post Content</div>
                        <textarea name="body" id="body" cols="30" rows="10" class="form-control"></textarea>
                        @if($errors->has('body'))
                            <span class="text-danger">Dit veld is verplicht.</span>
                        @endif
                    </div>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Create</button>
                </div>
            </div>
        </form>
        {{--End create Post--}}

    </div>

@endsection
@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">

        @include('inc.alerts')

        {{--Begin table Post--}}
        <div class="card">
            <h6 class="card-header text-muted">Alle posts</h6>
            <div class="card-body">
                <table class="table table-sm table-hover">
                    <thead>
                    <tr>
                        <th scope="col" width="30">Id</th>
                        <th scope="col" width="120">Foto</th>
                        <th>Post titel</th>
                        <th>Post content</th>
                        <th scope="col" width="80">Categorie</th>
                        <th scope="col" width="120">Controls</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->id }}</td>
                        <td><img src="/images/{{ isset($post->photos->path) ? $post->photos->path : ''  }}" alt="image" class="card-img" style="width: 100px"></td>
                        <td>{{ $post->title }}</td>

                        <td>{{ strip_tags(str_limit($post->body)) }}</td>
                        <td>{{ $categories->where('id', $post->category_id)->first() != null ? $categories->where('id', $post->category_id)->first()->name : '' }}</td>
                        <td>
                            <form action="{{ route('posts.destroy', $post->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                                <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{--End table Post--}}

    </div>
@endsection
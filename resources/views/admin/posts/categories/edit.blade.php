@extends('layouts.admin')

@section('content')
    <div class="col-md-10 mt-3">

        {{--Edit Categories--}}
        <form action="{{ route('categories.update', $categorie->id) }}" method="post">
            {{ csrf_field() }}
            @method('PUT')
            <div class="card">
                <h6 class="card-header text-muted">Edit categorie</h6>
                <div class="card-body">
                    <div class="card-title">Categorie naam</div>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" value="{{ old('name', $categorie->name)  }}">
                    </div>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Update</button>
                </div>
            </div>
        </form>
        {{--End edit Categories--}}
    </div>
@endsection
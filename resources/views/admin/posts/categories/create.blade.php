@extends('layouts.admin')

@section('content')
    <div class="col-md-10 mt-3">

        {{--Create Categories--}}
        <form action="{{ route('categories.store') }}" method="post">
            {{ csrf_field() }}
            <div class="card">
                <h6 class="card-header text-muted">Nieuwe categorie</h6>
                <div class="card-body">
                    <div class="card-title">Categorie naam</div>
                    <div class="form-group">
                        <input type="text" name="name" class="form-control">
                        @if($errors->has('name'))
                            <span class="text-danger">Naam is verplicht!</span>
                            @endif
                    </div>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Create</button>
                </div>
            </div>
        </form>

        {{--End create Categories--}}
    </div>
@endsection
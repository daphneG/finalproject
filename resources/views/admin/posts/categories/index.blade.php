@extends('layouts.admin')

@section('content')
    <div class="col-md-10 mt-3">
        @include('inc.alerts')

        {{--Table Categories--}}
        <div class="card">
            <h6 class="card-header text-muted">Alle categorie&euml;n</h6>
            <div class="card-body">
                <table class="table table-sm table-hover">
                    <thead>
                    <tr>
                        <th scope="col" width="30">Id</th>
                        <th>Categorie naam</th>
                        <th scope="col" width="120">Controls</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($categories as $categorie)
                    <tr>
                        <td>{{ $categorie->id }}</td>
                        <td>{{ $categorie->name }}</td>
                        <td>
                            <form action="{{ route('categories.destroy', $categorie->id) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <a href="{{ route('categories.edit', $categorie->id) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                                <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{--End table Categories--}}
    </div>
@endsection
@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">

        {{--Begin Edit Post--}}
        <form action="{{ route('posts.update', $post->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @method('PUT')
            <div class="card">
                <h6 class="card-header text-muted">Edit posts</h6>
                <div class="card-body">
                    <div class="card-title">Post titel</div>
                    <div class="form-group">
                        <input type="text" name="title" class="form-control" value="{{ $post->title }}">
                    </div>
                    <div class="card-title">Categorie&euml;n</div>
                    <div class="form-group">
                        <select name="category_id" id="category" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}" {{ isset($category->first()->id) ? ($category->id === $category->first()->id ? 'selected' : '') : ''}}>{{ $category->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <div class="card-title">Post Content</div>
                        <textarea name="body" id="body" cols="30" rows="10" class="form-control">{{ $post->body }}</textarea>
                    </div>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Update</button>
                </div>
            </div>
        </form>

        {{--End edit Post--}}
    </div>

@endsection
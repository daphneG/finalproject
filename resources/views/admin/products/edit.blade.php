@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">

        {{--Edit Product--}}
        <form action="{{ route('products.update', $product->id) }}" method="POST">
            {{ csrf_field() }}
             @method('PUT')
            <div class="card">
                <h6 class="card-header text-muted">Edit product</h6>
                <div class="card-body">
                    <h6 class="card-title">Ingredi&euml;nten naam</h6>
                    <div class="form-group">

                        <input name="name" type="text" id="name" class="form-control" value="{{ old('name') != null ? old('name') : (isset($product->name) ? $product->name : '') }}">
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Edit</button>
                </div>
            </div>
        </form>
        {{--End create Product--}}
    </div>
@endsection
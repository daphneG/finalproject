@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">

        {{--Create Product--}}
        <form action="{{ route('products.store') }}" method="POST">
            {{ csrf_field() }}
            <div class="card">
                <h6 class="card-header text-muted">Nieuw product</h6>
                <div class="card-body">
                    <h6 class="card-title">Ingredi&euml;nten naam</h6>
                    <div class="form-group">
                        <input name="name" type="text" id="name" class="form-control">
                        @if($errors->has('name'))
                            <span class="text-danger">Naam is verplicht.</span>
                        @endif
                    </div>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Create</button>
                </div>
            </div>
        </form>
        {{--End create Product--}}
    </div>
    @endsection
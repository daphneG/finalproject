@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">
        @include('inc.alerts')

        {{--Table Prodcuts--}}
        <div class="card">
            <h6 class="card-header text-muted">Alle Producten</h6>
            <div class="card-body">
                <table class="table table-sm table-hover">
                    <thead>
                    <tr>
                        <th scope="col" width="30">#</th>
                        <th scope="col">Product Naam</th>
                        <th scope="col" width="120">Controls</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach( $products as $product)
                            <tr>
                                <td> {{ $loop->iteration }} </td>
                                <td> {{ isset($product->name) ? $product->name : '' }} </td>
                                <td>
                                    <form action="{{ route('products.destroy', $product->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{ route('products.edit', $product->id) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                                        <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $products->links() }}
            </div>
        </div>
        {{--End table Products--}}
    </div>
@endsection
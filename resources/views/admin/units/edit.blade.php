@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">

        {{--Edit Unit--}}
        <form action="{{ route('units.update', $unit->id) }}" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            <div class="card">
                <h6 class="card-header text-muted">Edit eenheid</h6>
                <div class="card-body">
                    <h5 class="card-title">Eenheden</h5>
                    <div class="form-group">
                        <input name="units" id="units" class="form-control" value="{{ old('units') != null ? old('units') : (isset($unit->units) ? $unit->units : '') }}">
                        @if($errors->has('units'))
                            <span class="text-danger">{{ $errors->first('units') }}</span>
                        @endif
                    </div>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Update</button>
                </div>
            </div>
        </form>
        {{--End edit Unit--}}
    </div>

@endsection
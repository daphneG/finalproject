@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">
        @include('inc.alerts')

        {{--Table Units--}}
        <div class="card">
            <h6 class="card-header text-muted">Alle Eenheden</h6>
            <div class="card-body">
                <table class="table table-sm table-hover">
                    <thead>
                    <tr>
                        <th scope="col" width="30">#</th>
                        <th scope="col">Unit</th>
                        <th scope="col" width="120">Controls</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $units as $unit)
                        <tr>
                            <td> {{ $loop->iteration }} </td>
                            <td> {{ isset($unit->units) ? $unit->units : '' }} </td>
                            <td>
                                <form action="{{ route('units.destroy', $unit->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <a href="{{ route('units.edit', $unit->id) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                                    <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $units->links() }}
            </div>
        </div>
        {{--End table Units--}}
    </div>
@endsection
@extends('layouts.admin')

@section('content')

<div class="col-md-10 mt-3">

    {{--Create Unit--}}
    <form action="{{ route('units.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="card">
            <h6 class="card-header text-muted">Nieuwe eenheid</h6>
            <div class="card-body">
                <h6 class="card-title">Eenheden</h6>
                <div class="form-group">
                    <input name="units" id="units" type="text" class="form-control">
                    @if($errors->has('units'))
                        <span class="text-danger">Veld is verplicht.</span>
                    @endif
                </div>
                <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Create</button>
            </div>
        </div>
    </form>
    {{--End create Unit--}}
</div>

@endsection
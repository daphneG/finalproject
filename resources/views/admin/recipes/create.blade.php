@extends('layouts.admin')

@section('content')
<div class="col-md-10 mt-3">

    {{--Create Recipe--}}
    <form action="{{ route('recipes.store') }}" method="post" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="card">
            <h6 class="card-header text-muted">Nieuw recept</h6>
            <div class="card-body">
                <h6 class="card-title">Recepten naam</h6>
                <div class="form-group">
                <input type="text" class="form-control" name="name"><br>
                @if($errors->has('name'))
                    <span class="text-danger">Naam is verplicht.</span>
                @endif
                </div>
                <div class="form-group">
                    <input type="file" name="file">
                    @if($errors->has('file'))
                        <span class="text-danger">Foto uploaden is verplicht.</span>
                    @endif
                </div>
                <h6 class="card-title">Beschrijving</h6>
                <div class="form-group">
                <textarea name="desc" id="desc" cols="30" rows="5" class="form-control"></textarea>
                    @if($errors->has('desc'))
                        <span class="text-danger">Veld is verplicht.</span>
                    @endif
                </div>
                <h6 class="card-title">Categorie</h6>
                <div class="form-group">
                    <select name="sort" id="sort" class="form-control" >
                        <option value="ontbijt">ontbijt</option>
                        <option value="lunch">Lunch</option>
                        <option value="salade">Salade</option>
                        <option value="voorgerecht">Voorgerecht</option>
                        <option value="hoofdgerecht">Hoofdgerecht</option>
                        <option value="Bijgerecht">Bijgerecht</option>
                        <option value="dessert">Dessert</option>
                        <option value="gebak">Gebak & Taarten</option>
                    </select>
                    @if($errors->has('sort'))
                        <span class="text-danger">Veld is verplicht.</span>
                    @endif
                </div>
                <h6 class="card-title">Bereidings tijd</h6>
                <div class="form-group">
                    <input type="text" class="form-control" name="time">
                    @if($errors->has('time'))
                        <span class="text-danger">Veld is verplicht.</span>
                    @endif
                </div>
                <h6 class="card-title">Aantal Personen</h6>
                <div class="form-group">
                    <input type="text" class="form-control" name="number">
                    @if($errors->has('number'))
                        <span class="text-danger">Veld is verplicht.</span>
                    @endif
                </div>
                <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Create</button>
            </div>
        </div>
    </form>
    {{--End create Recipe--}}
</div>


    @endsection
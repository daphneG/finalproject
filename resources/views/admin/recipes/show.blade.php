@extends('layouts.app')

@include('inc.navbar')
@section('content')
<div class="col mt-3">
    {{--Show Recipe--}}
        <div class="card">
            <div class="card-body">
                <div class="img"></div>
                <img src="/images/{{ isset($recipe->photos->path) ? $recipe->photos->path : ''  }}" alt="image" class="show_img">
                <p></p>
                <h2 class="card-title" style="min-height: 50px;">{{ $recipe->name }}</h2>
                <p class="card-text" style="min-height: 30px;">{{ $recipe->desc }}</p>
                <br>
                <p><i class="far fa-clock"></i> {{ $recipe->time }}</p>
                <p><i class="fas fa-user"></i> {{ $recipe->number }} personen</p>
                <br>
                <h5 class="card-title">Ingredi&euml;nten</h5>
                <ul>
                    @foreach($recipe->ingredients as $ingredient)
                        <li><p>{{ floatval($ingredient->quantity) }} {{ $ingredient->units->first()['units'] }} {{ $ingredient->products->first()['name'] }}</p></li>
                    @endforeach
                </ul>
                <br>
                <h5 class="card-title">Bereiding</h5>
                @foreach($recipe->steps as $step)
                <p class="card-text">{{ $step->desc }}</p>
                @endforeach
                <a href="{{ route('welcome') }}" class="btn btn-outline-info btn-sm">Terug naar Home</a>
            </div>
        </div>
    {{--End show Recipe--}}
</div>
    @endsection
@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">
        {{--Edit Step--}}
        <form action="{{ route('steps.update', [$recipe->id, $steps->id]) }}" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            <div class="card">
                <h6 class="card-header text-muted">Edit stap</h6>
                <div class="card-body">
                    <h5 class="card-title">Stappen</h5>
                    <div class="form-group">
                        <textarea type="text" name="desc" id="desc" cols="30" rows="5" class="form-control">{{ old('desc') != null ? old('desc') : (isset($steps->desc) ? $steps->desc : '') }}</textarea>
                    </div>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Update</button>
                </div>
            </div>
        </form>
        {{--End edit Step--}}
    </div>

@endsection
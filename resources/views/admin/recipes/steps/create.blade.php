@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">
        {{--Create Step--}}
        <form action="{{ route('steps.store' , $recipe->id) }}" method="POST">
            {{ csrf_field() }}
            <div class="card">
                <h6 class="card-header text-muted">Nieuwe stap</h6>
                <div class="card-body">
                    <h5 class="card-title">Stappen</h5>
                    <div class="form-group">
                        <textarea type="text" name="desc" id="desc" cols="30" rows="5" class="form-control"></textarea>
                        @if($errors->has('desc'))
                            <span class="text-danger">Veld is verplicht.</span>
                            @endif
                    </div>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Create</button>
                </div>
            </div>
        </form>
        {{--End create Step--}}
    </div>

@endsection
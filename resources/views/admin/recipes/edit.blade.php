@extends('layouts.admin')

@section('content')
    <div class="col-md-10 mt-3">
        {{--Edit Recipe--}}
        <form action="{{ route('recipes.update', $recipe->id) }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            @method('PUT')
            <div class="card">
                <h6 class="card-header text-muted">Edit recept</h6>
                <div class="card-body">
                    <h6 class="card-title">Recepten naam</h6>
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" value="{{ $recipe->name }}">
                        @if($errors->has('name'))
                            <span class="text-danger">{{ $errors->first('name') }}</span>
                        @endif
                    </div>
                    <h6 class="card-title">Beschrijving</h6>
                    <div class="form-group">
                        <textarea name="desc" id="desc" cols="30" rows="5" class="form-control">{{ $recipe->desc }}</textarea>
                    </div>
                    <h6 class="card-title">Soort</h6>
                    <div class="form-group">
                        <select name="sort" id="sort" class="form-control">
                            <option value="ontbijt" {{$recipe->sort === 'ontbijt' ? 'Selected' : '' }}>Ontbijt</option>
                            <option value="lunch" {{$recipe->sort === 'lunch' ? 'Selected' : '' }}>Lunch</option>
                            <option value="salade" {{$recipe->sort === 'salade' ? 'Selected' : '' }}>Salade</option>
                            <option value="voorgerecht" {{$recipe->sort === 'voorgerecht' ? 'Selected' : '' }}>Voorgerecht</option>
                            <option value="hoofdgerecht" {{$recipe->sort === 'hoofdgerecht' ? 'Selected' : '' }}>Hoofdgerecht</option>
                            <option value="bijgerecht" {{$recipe->sort === 'bijgerecht' ? 'Selected' : '' }}>Bijgerecht</option>
                            <option value="dessert" {{$recipe->sort === 'dessert' ? 'Selected' : '' }}>Dessert</option>
                            <option value="gebak" {{$recipe->sort === 'gebak' ? 'Selected' : '' }}>Gebak & Taarten</option>
                        </select>
                    </div>
                    <h6 class="card-title">Bereidings tijd</h6>
                    <div class="form-group">
                        <input type="text" class="form-control" name="time" value="{{ $recipe->time }}">
                    </div>
                    <h6 class="card-title">Aantal Personen</h6>
                    <div class="form-group">
                        <input type="text" class="form-control" name="number" value="{{ $recipe->number }}">
                    </div>

                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Update</button>
                </div>
            </div>
        </form>
       {{-- End edit Recipe--}}


        <div class="card mt-3">
            {{--Add new Product--}}
            <h6 class="card-header text-muted">Ingredi&euml;nten</h6>
            <div class="card-body">
                <table class="table table-sm table-hover">
                    <thead>
                    <tr>
                        <th>Naam</th>
                        <th>Hoeveelheid</th>
                        <th>Eenheid</th>
                        <th scope="col" width="120">Controls</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($quantities as $quantity)
                    <tr>
                        <td>
                            {{ $quantity->products->first()['name'] }}
                        </td>
                        <td>
                            {{ floatval($quantity->quantity) }}
                        </td>
                        <td>
                            {{ $quantity->units->first()['units'] }}
                        </td>
                        <td>
                            <form action="{{ route('ingredients.destroy', [$recipe, $quantity]) }}" method="post">
                                {{ csrf_field() }}
                                @method('DELETE')
                                <a href="{{ route('ingredients.edit', [$recipe, $quantity]) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                                <button class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $quantities->links() }}
                <div class="text-right">
                    <a href="{{ route('ingredients.create', $recipe->id) }}" class="btn btn-sm btn-outline-secondary">Ingredi&euml;nt toevoegen</a>
                </div>
            </div>
            {{--End new Product--}}
        </div>



        <div class="card mt-3">
            {{--Add new Steps--}}
            <h6 class="card-header text-muted">Stappen</h6>
            <div class="card-body">
                <table class="table table-sm table-hover">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Stappen</th>
                        <th scope="col" width="120">Controls</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach( $steps as $step)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ str_limit($step->desc) }}</td>
                        <td>
                            <form action="{{ route('steps.destroy', [$recipe->id, $step->id]) }}" method="POST">
                                {{ csrf_field() }}
                                @method('DELETE')
                                <a href="{{ route('steps.edit', [$recipe->id, $step->id]) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                                <button class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                        </td>
                    </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="text-right">
                    <a href="{{ route('steps.create', $recipe->id) }}" class="btn btn-sm btn-outline-secondary">Stap toevoegen</a>
                </div>
            </div>
        </div>
        {{--End new Steps--}}
    </div>


@endsection
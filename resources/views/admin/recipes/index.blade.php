@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">
        @include('inc.alerts')

        {{--Table recipes--}}
        <div class="card">
            <h6 class="card-header text-muted">Alle Recepten</h6>
            <div class="card-body">
                <table class="table table-sm table-hover">
                    <thead>
                    <tr>
                        <th scope="col" width="30">Id</th>
                        <th>Naam</th>
                        <th scope="col" width="150">Foto</th>
                        <th scope="col" width="120">Controls</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($recipes as $recipe)
                        <tr>
                            <td>{{ $recipe->id }}</td>
                            <td>{{ $recipe->name }}</td>
                            <td><img src="/images/{{ isset($recipe->photos->path) ? $recipe->photos->path : ''  }}" alt="image" class="card-img" style="width: 80px;"></td>
                            <td>
                                <form action="{{ route('recipes.destroy', $recipe->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <a href="{{ route('recipes.edit', $recipe->id) }}" class="btn btn-outline-secondary btn-sm">Edit</a>
                                    <button type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $recipes->links() }}
            </div>
        </div>
        {{--End table Recipes--}}
    </div>

    @endsection
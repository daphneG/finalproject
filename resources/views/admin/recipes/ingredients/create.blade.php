@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">
        {{--Create Ingredients--}}
        <form action="{{ route('ingredients.store', $recipe->id) }}" method="post">
            {{ csrf_field() }}
            <div class="card">
                <h6 class="card-header text-muted">Nieuw Ingredi&euml;nt</h6>
                <div class="card-body">
                    <h6 class="card-title">Ingredi&euml;nt</h6>
                    <input list="listProducts" name="product_name" class="form-control">
                    <datalist id="listProducts">
                        @foreach($products as $product)
                            <option value="{{ $product->name }}">{{ $product->name }}</option>
                        @endforeach
                        <option value="Safari">
                    </datalist>
                    <br>
                    <h6 class="card-title">Hoeveelheid</h6>
                    <div class="form-group">
                        <input type="text" class="form-control" name="quantity" id="quantity"><br>
                        @if($errors->has('quantity'))
                            <span class="text-danger">Veld is verplicht.</span>
                        @endif
                    </div>
                    <h6 class="card-title">Eenheid</h6>
                    <div class="form-group">
                        <select name="unit_id" id="units" class="form-control">
                            @foreach($units as $unit)
                                <option value="{{ $unit->id }}">{{ $unit->units }}</option>
                            @endforeach
                        </select>
                    </div>

                    <br>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Create</button>
                </div>
            </div>
        </form>
        {{--End create ingredients--}}
    </div>

    @endsection
@extends('layouts.admin')

@section('content')

    <div class="col-md-10 mt-3">
        {{--Edit Ingredients--}}
        <form action="{{ route('ingredients.update', [$recipe, $ingredient]) }}" method="post">
            {{ csrf_field() }}
            @method('PUT')
            <div class="card">
                <h6 class="card-header text-muted">Edit Ingredi&euml;nt</h6>
                <div class="card-body">
                    <h6 class="card-title">Ingredi&euml;nt</h6>
                    <input list="listProducts" name="product_name" class="form-control" value="{{ old('product_name') ? old('product_name') : (isset($ingredient->products->first()->name) ? $ingredient->products->first()->name : '') }}">

                    <datalist id="listProducts">
                        @foreach($products as $product)
                            <option value="{{ $product->name }}"></option>
                        @endforeach
                        <option value="Safari">
                    </datalist>
                    <br>
                    <h6 class="card-title">Hoeveelheid</h6>
                    <input type="text" class="form-control" name="quantity" id="quantity" value="{{ old('quantity') ? old('quantity') : (isset($ingredient->quantity) ? $ingredient->quantity : '') }}"><br>
                    @if($errors->has('quantity'))
                        <span class="text-danger">{{ $errors->first('quantity') }}</span>
                    @endif
                    <h6 class="card-title">Eenheid</h6>
                    <select name="unit_id" id="units" class="form-control">
                        @foreach($units as $unit)
                            <option value="{{ $unit->id }}" {{ isset($ingredient->units->first()->id) ? ($unit->id === $ingredient->units->first()->id ? 'selected' : '') : ''}}>{{ $unit->units }}</option>
                        @endforeach
                    </select><br>
                    <button class="btn btn-outline-primary btn-sm" type="submit" name="submit">Update</button>
                </div>
            </div>
        </form>
        {{--End edit Ingredients--}}
    </div>

@endsection
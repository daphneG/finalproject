@extends('layouts.app')

@include('inc.navbar')

@section('content')
    @include('inc.alerts')

    <main class="col-sm-12">
        <div class="row justify-content-md-center">
            <div class="col-sm-12 mt-3">
                <div class="well">
                    <form action="{{ route('search') }}" method="post">
                        {{ csrf_field() }}
                        <div class="input-group">
                            <input name="search" type="text" class="form-control" placeholder="recepten naam">
                            <span class="form-inline">
                                <button name="submit" class="btn btn-outline-light my-2 my-sm-0" type="submit">Search</button>
                            </span>
                        </div>
                    </form>
                    <select name="recipeDropdown" id="recipeDropdown">
                        <option>filter recepten</option>
                        <option value="ontbijt">Ontbijt</option>
                        <option value="lunch">Lunch</option>
                        <option value="salade">Salades</option>
                        <option value="voorgerecht">Voorgerechten</option>
                        <option value="hoofdgerecht">Hoofdgerechten</option>
                        <option value="bijgerecht">Bijgerechten</option>
                        <option value="dessert">Dessert</option>
                        <option value="gebak">Gebak & Taarten</option>
                    </select>
                </div>
                <h1 class="text-center">Welkom bij Daphne's recepten wereld!</h1>
                <hr>
            </div>

            @foreach($recipes as $recipe)
            <div class="col-md-3 col-lg-2 col-xl-3 recepten {{$recipe->sort}}">
                <br>
                <div class="card">
                    <img src="/images/{{ isset($recipe->photos->path) ? $recipe->photos->path : ''  }}" alt="image" class="welcome_img" >
                    <div class="card-body">
                        <h5 class="card-title" style="min-height: 45px;">{{ $recipe->name }}</h5>
                        <p class="card-text" style="min-height: 90px;">{{ str_limit($recipe->desc) }}</p>
                        <a href="{{ route('gerechten.show', $recipe->id) }}">Lees meer</a>

                        @if (Auth::check())
                            @php $check = false; @endphp
                            @foreach($favorites as $favorite)
                                @if($favorite->recipe_id === $recipe->id)
                                    @php $check = true; @endphp
                                @endif
                            @endforeach
                            @if(!$check)
                                <form action="{{ route('favorites.store', $recipe) }}" method="POST">
                                    @csrf
                                    <button class="btn btn-danger btn-sm" style="float: right;">Add to <i class="far fa-fw fa-heart"></i></button>
                                </form>
                            @endif
                        @endif
                    </div>
                </div>
                <br>
            </div>
                @endforeach
        </div>
    </main>


@endsection



import '../../../node_modules/bootstrap/dist/js/bootstrap';

(function($) {
    "use strict"; // Start of use strict

    $(document).ready(function () {
        $(".alert").fadeTo(2000, 500).slideUp(500, function(){ 
            $(".alert").slideUp(500); 
        });

        $("#recipeDropdown").change(function(){
           $(".recepten").hide();
           $("." + $(this).val()).show();
        });
    });
})(jQuery);

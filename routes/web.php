<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'PublicController@showIndex')->name('welcome');

Auth::routes();

//Route::resource('/user/favorites', 'FavoriteController')->except('show', 'create', 'edit', 'update');
Route::get('/user/favorites/{recipe}', 'FavoriteController@index')->name('favorites.index');
Route::post('/user/favorites/{recipe}', 'FavoriteController@store')->name('favorites.store');
Route::delete('/user/favorites/{favorite}', 'FavoriteController@destroy')->name('favorites.destroy');


Route::get('/admin', 'HomeController@admin')->name('admin');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/gerechten', 'PublicController@gerechten')->name('gerechten');
Route::get('/gerechten/{recipe}', 'PublicController@show')->name('gerechten.show');
Route::get('/posts/showIndex', 'PublicController@showPosts')->name('posts.showIndex');
Route::get('/post/{post}', 'PublicController@showPost')->name('post.detail');
Route::resource('/post/{post}/comments', 'CommentController')->except('index', 'create', 'show', 'edit', 'update');
Route::post('/searchRecipe', 'PublicController@searchRecipe')->name('search');
Route::post('/searchPost', 'PublicController@searchPost')->name('posts.search');
Route::get('/top5', 'PublicController@top5')->name('top5');
Route::get('/about', 'PublicController@about')->name('about');

Route::resource('/admin/recipes', 'RecipeController')->except('show');
Route::resource('/admin/recipes/{recipe}/ingredients', 'IngredientController')->except('index', 'show');
Route::resource('/admin/products', 'ProductController')->except('show');
Route::resource('/admin/units', 'UnitController')->except('show');
Route::resource('/admin/recipes/{recipe}/steps', 'StepController')->except('index','show');
Route::resource('/admin/posts', 'PostController');
Route::resource('/admin/categories', 'CategoryController')->except('show');
Route::resource('/contacts', 'ContactController')->except('show', 'edit', 'update');



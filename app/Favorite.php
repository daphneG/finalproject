<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Favorite extends Model
{
    public function users(){

        return $this->belongsTo(User::class);
    }
}

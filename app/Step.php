<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Step extends Model
{
    protected $fillable = ['steps'];

    public function recipes(){

        return $this->belongsTo('App\Recipe');
    }
}

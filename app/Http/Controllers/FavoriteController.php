<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\Recipe;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Recipe $recipe, Request $request)
    {
        $user = Auth::user();

        if($user->favorites()->where('recipe_id', $recipe->id)->first() === null){
            $favorite = new Favorite;
            $favorite->user_id = $user->id;
            $favorite->recipe_id = $recipe->id;

            $user->favorites()->save($favorite);

            return back()->with('success', 'Added to Favorites!');
        }else{
            return back()->with('failed', 'Already a Favorite');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe, Favorite $favorite)
    {
        $user = Auth::check() ? User::find(Auth::user()->id) : '';

        $user->favorites()->find($favorite->id)->delete();

        //$favorite->delete();

        return redirect()->back();
    }
}

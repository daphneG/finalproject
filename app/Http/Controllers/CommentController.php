<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param Post $post_id
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(Post $post, Request $request)
    {

        $comment = new Comment;
        //$comment->post_id = $post->id;
        $comment->name = $request->name;
        $comment->email = $request->email;
        $comment->comment = $request->comment;

        $post->comments()->save($comment);

        return redirect()->back()->with('success', 'Inserted successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Post $post
     * @param  int $id
     * @return void
     */
    public function destroy(Post $post, $id)
    {
        $post->comments->find($id)->delete();

        return redirect()->back()->with('success', 'Deleted successfully');
    }
}

<?php

namespace App\Http\Controllers;

use App\Category;
use App\Ingredient;
use App\Post;
use App\Product;
use App\Recipe;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PublicController extends Controller
{
    public function showIndex(){
        $recipes = Recipe::orderBy('id', 'desc')->get();
        $user = Auth::check() ? User::findOrFail(Auth::user()->id): null;
        $favorites = $user !== null ? $user->getAllFavorites() : null;

        //dd($favorites);
        return view('/welcome' , compact('recipes', 'favorites'));
    }

    public function gerechten(){

        $recipes = Recipe::all();

        return view('/gerechten', compact('recipes'));
    }

    /**
     * Display the specified resource.
     *
     * @param Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function show(Recipe $recipe, Request $request)
    {
        $ingredients = Ingredient::where('recipe_id', '=', $recipe->id);
        $products = Product::all();
        $units = Unit::all();
        $steps = $recipe->steps;

        return view('admin/recipes/show', compact('recipe', 'products', 'ingredients', 'units', 'steps'));
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showPost(Post $post)
    {
        $categories = Category::all();
        $comments = $post->comments;

        return view('posts/show', compact('post', 'categories', 'comments'));
    }

    public function showPosts()
    {
        $posts = Post::orderBy('id', 'desc')->paginate(6);
        $categories = Category::all();

        return view('posts/showIndex', compact('posts', 'categories'));
    }

    public function searchRecipe(Request $request){
        $search = $request->search;
        $recipes = Recipe::where('name', 'like', '%' . $search . '%')->get();

        $favorites = Auth::check() ? User::findOrFail(Auth::user()->id)->getAllFavorites() : '';

        return view('search', compact('recipes', 'favorites'));
    }

    public function searchPost(Request $request){
        $search = $request->search;

        $posts = Post::where('title', 'like', '%' . $search . '%')
                ->orWhere('body', '=', '%' . $search . '%')->get();

        return view('posts/search', compact('posts'));
    }

    public function Top5(Recipe $recipe){
        $recipe1 = Recipe::find([11]);
        $recipe2 = Recipe::find([9]);
        $recipe3 = Recipe::find([5]);
        $recipe4 = Recipe::find([6]);
        $recipe5 = Recipe::find([3]);
        $ingredients = Ingredient::where('recipe_id', '=', $recipe->id);
        $products = Product::all();
        $units = Unit::all();
        $steps = $recipe->steps;

        return view('/top5', compact('recipe1', 'recipe2', 'recipe3','recipe4', 'recipe5',  'ingredients', 'products', 'units', 'steps'));
    }

    public function about(){

        return view('/about');
    }
}

<?php

namespace App\Http\Controllers;

use App\Favorite;
use App\Recipe;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $favorites = Auth::user()->getAllFavorites();
        $recipes = Recipe::all();
        //dd($favorites);
        return view('home',compact('favorites', 'recipes'));
    }

    public function admin(){
        return view('admin/dashboard');
    }



}

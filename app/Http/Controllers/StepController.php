<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreStepRequest;
use App\Recipe;
use App\Step;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class StepController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($recipe_id)
    {
        $recipe = Recipe::find($recipe_id);
        return view('admin/recipes/steps/create', compact('recipe'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( $recipe_id ,StoreStepRequest $request)
    {
        $steps = new Step;
        $steps->desc = $request->input('desc');

        $recipe = Recipe::find($recipe_id);
        $recipe->steps()->save($steps);

        return redirect()->route('recipes.edit', $recipe->id)->with('success', 'Inserted successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($recipe_id, $steps_id)
    {
        $recipe = Recipe::findOrFail($recipe_id);
        $steps = Step::findOrFail($steps_id);

        return view('admin/recipes/steps/edit', compact('steps', 'recipe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Recipe $recipe, $step_id)
    {

        $step = $recipe->steps()->findOrFail($step_id);
        $step->desc = $request->desc;
        $step->save();

        return redirect()->route('recipes.edit', $recipe->id)->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe, $steps_id)
    {
        $recipe->steps()->findOrFail($steps_id)->delete();

        return redirect()->route('recipes.edit', $recipe->id)->with('success', 'Deleted successfully');
    }
}

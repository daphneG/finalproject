<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreIngredientRequest;
use App\Ingredient;
use App\Product;
use App\Recipe;
use App\Unit;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;

class IngredientController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function create(Recipe $recipe)
    {
        $products = Product::all();
        $units = Unit::all();

        return view('admin/recipes/ingredients/create', compact('recipe', 'products', 'units'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Recipe $recipe
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Recipe $recipe, StoreIngredientRequest $request)
    {
        $quantities = new Ingredient;
        $quantities->quantity = $request->input('quantity');
        $recipe->ingredients()->save($quantities);

        $product = Product::all()->where('name' , '=', $request->product_name);

        if ($product->first() != null) {
            $quantities->products()->attach($product->first()->id);
        }else{
            $new_product = new Product;
            $new_product->name = $request->product_name;
            $new_product->save();
            $quantities->products()->attach($new_product->id);
        }

        $quantities->units()->attach($request->unit_id);

        return redirect()->route('recipes.edit', $recipe->id)->with('success', 'Inserted successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Ingredient $ingredient
     * @param Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe, Ingredient $ingredient )
    {
        $products = Product::all();
        $units = Unit::all();

        return view('admin/recipes/ingredients/edit', compact('ingredient', 'recipe', 'products', 'units'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Ingredient $ingredient
     * @param Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function update(Recipe $recipe, Ingredient $ingredient, Request $request)
    {

        $product_id = $ingredient->products->first()->id;
        $product_name = $ingredient->products->first()->name;
        $unit_id = $ingredient->units->first()->id;

        if($ingredient->quantity != $request->quantity){
            $ingredient->quantity = $request->quantity;
            $ingredient->update();
        }

        if($request->product_name != $product_name){




            if ($product = Product::all()->where('name' , '=', $request->product_name)->first()) {

                $ingredient->products()->toggle([$product_id, $product->id]);

            }else{
                $new_product = new Product;
                $new_product->name = $request->product_name;
                $new_product->save();
                $ingredient->products()->toggle([$product_id, $new_product->id]);
            }
        }

        if($request->unit_id != $unit_id){
            $ingredient->units()->detach($unit_id);
            $ingredient->units()->attach($request->unit_id);
        }

        return redirect()->route('recipes.edit', $recipe)->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Recipe $recipe
     * @param Ingredient $ingredient
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipe $recipe, Ingredient $ingredient, Request $request)
    {
        if($ingredient->products->first() != null){
            $product_id = $ingredient->products->first()->id;
            $ingredient->products()->detach($product_id);
        }

        if($ingredient->units->first() != null){
            $unit_id = $ingredient->units->first()->id;
            $ingredient->units()->detach($unit_id);
        }

        $ingredient->delete();

        return redirect()->route('recipes.edit', $recipe)->with('success', 'Deleted successfully');
    }
}

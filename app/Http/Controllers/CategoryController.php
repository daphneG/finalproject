<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StoreCategoryRequest;
use App\Post;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {

        $categories = Category::all();
        return view( 'admin/posts/categories/index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/posts/categories/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function store(StoreCategoryRequest $request)
    {
        $categorie = new Category;
        $categorie->name = $request->input('name');

        $categorie->save();


        return redirect()->route('categories.index')->with('success', 'Inserted successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categorie = Category::findOrFail($id);

        return view('admin/posts/categories/edit', compact('categorie'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $categorie = Category::findOrFail($id);
        $categorie->name = $request->name;
        $categorie->update();

        return redirect()->route('categories.index')->with('success', 'Updated successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $categorie = Category::findOrFail($id);
        $categorie->delete();

        return redirect()->route('categories.index')->with('success', 'Deleted successfully');
    }
}

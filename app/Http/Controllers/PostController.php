<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StorePostRequest;
use App\Post;
use App\Photo;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('authAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::all();
        $categories = Category::all();

        return view('admin/posts/index', compact('posts', 'categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin/posts/create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $post = new Post;
        $photo = new Photo;

        $post->title = $request->input('title');
        $post->body = $request->input('body');

        if ($file = $request->file('file')) {

            $name = $file->getClientOriginalName();
            $file->move('images', $name);
            $photo->path = $name;
        }

        $post->category_id = $request->category_id;

        $post->save();
        $post->photos()->save($photo);

        return redirect()->route('posts.index')->with('success', 'Inserted successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        $categorie = Category::all();


        return view('admin/posts/show', compact('post', 'categorie'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $categories = Category::all();

        return view('admin/posts/edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id);
        $post->title = $request->input('title');
        $post->body = $request->input('body');
        $post->category_id = $request->category_id;
        $post->update();

        return redirect()->route('posts.index')->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->photos()->delete();
        $post->delete();

        return redirect()->route('posts.index')->with('success', 'Deleted successfully');
    }
}
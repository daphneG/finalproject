<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRecipeRequest;
use App\Ingredient;
use App\Photo;
use App\Product;
use App\Recipe;
use App\Step;
use App\Unit;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RecipeController extends Controller
{
    public function __construct()
    {
        $this->middleware('authAdmin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $recipes = Recipe::orderBy('id', 'desc')->paginate(8);

        Return view('admin/recipes/index', compact('recipes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->user()->authorizeRoles('admin');
        return view('admin/recipes/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRecipeRequest $request)
    {
        $request->user()->authorizeRoles('admin');

        $recipe = new Recipe;
        $photo = new Photo;

        $recipe->name = $request->input('name');
        $recipe->desc = $request->input('desc');
        $recipe->sort = $request->input('sort');
        $recipe->time = $request->input('time');
        $recipe->number = $request->input('number');

        if ($file = $request->file('file')){

            $name = $file->getClientOriginalName();
            $file->move('images', $name);
            $photo->path = $name;
        }

        $recipe->save();
        $recipe->photos()->save($photo);

        return redirect()->route('recipes.edit', $recipe->id)->with('success', 'Inserted successfully');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Recipe $recipe
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipe $recipe, Request $request)
    {
        $request->user()->authorizeRoles('admin');

        $quantities = Ingredient::where('recipe_id', '=', $recipe->id)->paginate(6);
        $products = Product::all();
        $units = Unit::all();
        $steps = $recipe->steps;
        return view('admin/recipes/edit', compact('recipe', 'steps', 'quantities', 'products', 'units', 'paginates'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->user()->authorizeRoles('admin');
        $recipe = Recipe::findOrFail($id);

        $recipe->update($request->only('name', 'desc', 'sort', 'time', 'number'));

        return redirect()->route('recipes.index')->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $request->user()->authorizeRoles('admin');
        $recipe = Recipe::findOrFail($id);
        $recipe->photos()->delete();

        foreach($recipe->ingredients as $ingredient){
            foreach($ingredient->units as $unit){
                $ingredient->units()->detach($unit->id);
            }
            foreach($ingredient->products as $product){
                $ingredient->products()->detach($product->id);
            }
        }

        $recipe->delete();

        return redirect()->route('recipes.index');
    }


}

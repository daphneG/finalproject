<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    protected $fillable = ['path'];

    public function recipes(){

        return $this->belongsTo('App\Recipe');
    }

    public function posts(){

        return $this->belongsTo(('App\Post'));
    }
}

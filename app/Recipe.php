<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    public $directory = "/images/";

    protected $fillable = ['name', 'desc', 'sort', 'time', 'number'];

    public function photos(){

        return $this->hasOne('App\Photo');
    }

    public function ingredients(){

        return $this->hasMany('App\Ingredient');
    }

    public function steps(){

        return $this->hasMany('App\Step');
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title', 'content'];

    public function photos(){

        return $this->hasOne('App\Photo');
    }

    public function categories(){

        return $this->hasMany('App\Category');
    }

    public function comments(){

        return $this->hasMany('App\Comment');
    }
}

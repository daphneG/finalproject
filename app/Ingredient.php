<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    protected $fillable = ['quantity'];

    public function recipes(){

        return $this->belongsTo('App\Recipe');
    }

    public function products(){

        return $this->belongsToMany('App\Product');
    }

    public function units(){

        return $this->belongsToMany('App\Unit');
    }
}
